﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
    public partial class  NadhodQuery : UserControl
    {
        SqlConnection connection;
        string connectionString;
        string date0;
        string date1;
        public  NadhodQuery()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        private void  NadhodQuery_Load(object sender, EventArgs e)
        {
            
            fmMain.Instance.Text = "        Поставки";
            fmMain.Instance.Refresh();
            PopulateQuery();
        }

        private void ReplaceHeaders()
        {
            mgData.Columns[0].HeaderText = "Дата";
            mgData.Columns[1].HeaderText = "Контерагент";
            mgData.Columns[2].HeaderText = "Артикул";
            mgData.Columns[3].HeaderText = "Товар";
            mgData.Columns[4].HeaderText = "Склад";
            mgData.Columns[5].HeaderText = "Кількість";
            mgData.Columns[6].HeaderText = "Ціна за од.";
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
        }

        private void PopulateQuery()
        {
            date0 = dateBegin.Value.ToString("yyyy.MM.dd");
            date1 = dateEnd.Value.ToString("yyyy.MM.dd");

            string query = "SELECT Nadhod.Date, Konteragent.Nazva, Nadhod.Artukyl, Tovaru.Nazva, Sklad.NazvaSkladu, Nadhod.Kilkist, Nadhod.CzinaZaOd" +
                    " FROM Nadhod" +
                    " INNER JOIN Tovaru ON Nadhod.Artukyl = Tovaru.Artukyl" +
                    " INNER JOIN Konteragent ON Nadhod.IdKonteragent = Konteragent.Id " +
                    " INNER JOIN Sklad ON Nadhod.IdSklad = Sklad.Id ";

            if (checkBetween.Checked)
            {
                query += " WHERE (Nadhod.Date BETWEEN @date0 AND @date1 ) ";
            }
            else {
                query += " WHERE (Nadhod.Date = @date0 )";
            }

            using (connection = new SqlConnection(connectionString))
            {
                using(SqlCommand command = new SqlCommand(query, connection)) { 
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                   command.Parameters.AddWithValue("date0", date0);
                   if (checkBetween.Checked)
                   {
                      command.Parameters.AddWithValue("date1", date1);
                   }
                    
                   DataTable table = new DataTable();
                   adapter.Fill(table);
                   mgData.DataSource = table;
                }

                }
            }
            ReplaceHeaders();
        }

        private void dateBegin_ValueChanged(object sender, EventArgs e)
        {
            PopulateQuery();
        }

        private void dateEnd_ValueChanged(object sender, EventArgs e)
        {
            PopulateQuery();
        }

        private void checkBetween_CheckedChanged(object sender, EventArgs e)
        {
            dateEnd.Enabled = checkBetween.Checked;
            PopulateQuery();
        }

        private void bReport_Click(object sender, EventArgs e)
        {

            Report rpt = new Report();
            //set 
            rpt.LoadNadhod(dateBegin.Value, dateEnd.Value);
            rpt.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
    public partial class StorageContain : UserControl
    {
        SqlConnection connection;
        string connectionString;
        string MVO;
        public StorageContain()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        private void StorageContain_Load(object sender, EventArgs e)
        {
            
            fmMain.Instance.Text = "        На складі";
            fmMain.Instance.Refresh();
            PopulateStorages();
        }

        private void PopulateStorages() {
            using (connection = new SqlConnection(connectionString) ) {
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Sklad", connection)) {
                    DataTable storageTable = new DataTable();
                    adapter.Fill(storageTable);
                   // MVO = storageTable.Col
                    cbStorage.DisplayMember = "NazvaSkladu";
                    cbStorage.ValueMember = "Id";
                    cbStorage.DataSource = storageTable;
                    cbStorage.SelectedIndex = 0;
                }

            }
        }

        private void ReplaceHeaders()
        {
            mgData.Columns[0].HeaderText = "Артикул";
            mgData.Columns[1].HeaderText = "Назва";
            mgData.Columns[2].HeaderText = "Од.";
            mgData.Columns[3].HeaderText = "Кількість";
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
        }

        private void PopulateProducts()
        {
            //feat Vova DRonenko
            string query = "SELECT Nadhod.Artukyl, Tovaru.Nazva, Tovaru.OdVumiry, SUM(Nadhod.Kilkist) FROM Nadhod " +
                "INNER JOIN Tovaru ON Nadhod.Artukyl = Tovaru.Artukyl " +
                "WHERE(Nadhod.IdSklad = @idSkladParameter) GROUP BY Nadhod.Artukyl, Tovaru.Nazva, Tovaru.OdVumiry";

            using (connection = new SqlConnection(connectionString))
            {
                using(SqlCommand command = new SqlCommand(query, connection)) { 
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                     
                   command.Parameters.AddWithValue("idSkladParameter", cbStorage.SelectedValue);
                 
                   DataTable dataTable = new DataTable();
                   adapter.Fill(dataTable);
                   mgData.DataSource = dataTable;
                    
                }

                }
            }
            ReplaceHeaders();
        }

        private void cbStorage_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

        private void cbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateProducts();
        }

        private void bReport_Click(object sender, EventArgs e)
        {
            Report rpt = new Report();
            //set storage id 
            rpt.LoadStorageContainReport(cbStorage.SelectedValue.ToString());
            rpt.ShowDialog();
        }
    }
}

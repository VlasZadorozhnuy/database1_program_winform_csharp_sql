﻿namespace ProbaCursApp
{
    partial class fmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mpPanel = new System.Windows.Forms.Panel();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.lAsterix = new MetroFramework.Controls.MetroLabel();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.mlBack = new MetroFramework.Controls.MetroLink();
            this.storageDataSet = new ProbaCursApp.StorageDataSet();
            this.skladBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.skladTableAdapter = new ProbaCursApp.StorageDataSetTableAdapters.SkladTableAdapter();
            this.tableAdapterManager = new ProbaCursApp.StorageDataSetTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storageDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skladBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mpPanel
            // 
            this.mpPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mpPanel.Location = new System.Drawing.Point(20, 60);
            this.mpPanel.Name = "mpPanel";
            this.mpPanel.Size = new System.Drawing.Size(760, 520);
            this.mpPanel.TabIndex = 0;
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = null;
            // 
            // lAsterix
            // 
            this.lAsterix.AutoSize = true;
            this.lAsterix.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lAsterix.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lAsterix.Location = new System.Drawing.Point(60, 22);
            this.lAsterix.Name = "lAsterix";
            this.lAsterix.Size = new System.Drawing.Size(20, 25);
            this.lAsterix.TabIndex = 2;
            this.lAsterix.Text = "*";
            // 
            // metroLink1
            // 
            this.metroLink1.Location = new System.Drawing.Point(0, 0);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.TabIndex = 0;
            this.metroLink1.UseSelectable = true;
            // 
            // mlBack
            // 
            this.mlBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.mlBack.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlBack.Image = global::ProbaCursApp.Properties.Resources.back1;
            this.mlBack.Location = new System.Drawing.Point(20, 22);
            this.mlBack.Name = "mlBack";
            this.mlBack.Size = new System.Drawing.Size(44, 32);
            this.mlBack.TabIndex = 1;
            this.mlBack.UseSelectable = true;
            this.mlBack.Click += new System.EventHandler(this.mlBack_Click);
            // 
            // storageDataSet
            // 
            this.storageDataSet.DataSetName = "StorageDataSet";
            this.storageDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // skladBindingSource
            // 
            this.skladBindingSource.DataMember = "Sklad";
            this.skladBindingSource.DataSource = this.storageDataSet;
            // 
            // skladTableAdapter
            // 
            this.skladTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.KonteragentTableAdapter = null;
            this.tableAdapterManager.NadhodTableAdapter = null;
            this.tableAdapterManager.NaSkladiTableAdapter = null;
            this.tableAdapterManager.SkladTableAdapter = this.skladTableAdapter;
            this.tableAdapterManager.TovaruTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ProbaCursApp.StorageDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserTableAdapter = null;
            // 
            // fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProbaCursApp.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.mlBack;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.lAsterix);
            this.Controls.Add(this.mlBack);
            this.Controls.Add(this.mpPanel);
            this.Name = "fmMain";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "        Облік товарів на складах";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storageDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skladBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel mpPanel;
        private MetroFramework.Controls.MetroLink mlBack;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Controls.MetroLabel lAsterix;
        private MetroFramework.Controls.MetroLink metroLink1;
        private StorageDataSet storageDataSet;
        private System.Windows.Forms.BindingSource skladBindingSource;
        private StorageDataSetTableAdapters.SkladTableAdapter skladTableAdapter;
        private StorageDataSetTableAdapters.TableAdapterManager tableAdapterManager;
    }
}


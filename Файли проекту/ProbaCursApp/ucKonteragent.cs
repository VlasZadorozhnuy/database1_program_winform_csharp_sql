﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace ProbaCursApp
{
    public partial class ucKonteragent : MetroUserControl
    {

        #region Init
        public ucKonteragent()
        {
            InitializeComponent();
        }


        private void ucKonteragent_Load(object sender, EventArgs e)
        {
            fmMain.Instance.Text = "        Контерагенти";
            fmMain.Instance.Refresh();
            this.konteragentTableAdapter.Fill(this.storageDataSet.Konteragent);
            Edit(false);
            //new headers text in grid
            ReplaceHeaders();
            //add headers values in combobox Search
            AddComboboxItems();
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
        }

        bool tableEdited = false;

        private void ReplaceHeaders()
        {
            mgData.Columns[0].HeaderText = "Назва контерагента";
        }

        private void AddComboboxItems()
        {
            for (int i = 0; i < mgData.Columns.Count; i++)
            {
                cbSearch.Items.Add(mgData.Columns[i].HeaderText);
            }
            cbSearch.SelectedIndex = 0;
        }    

        #endregion
        #region navigation logic

        private void Edit(bool value)
        {
            txName.Enabled = value;
            mlCancel.Enabled = true;
        }

        private void TableEdited(bool value)
        {
            mlCancel.Enabled = value;
            tableEdited = value;
            fmMain.Instance.LAsterix.Visible = value;
            
        }

        private void mlNew_Click(object sender, EventArgs e)
        {
            try
            {

                Edit(true);
                storageDataSet.Konteragent.AddKonteragentRow(storageDataSet.Konteragent.NewKonteragentRow());
                konteragentBindingSource.MoveLast();
                txName.Focus();
                TableEdited(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Konteragent.RejectChanges();
                //throw;
            }
        }

        private void mlEdit_Click(object sender, EventArgs e)
        {
            Edit(true);
            txName.Focus();
            TableEdited(true);
        }

        private void mlCancel_Click(object sender, EventArgs e)
        {
            Edit(false);
            konteragentBindingSource.CancelEdit();
            konteragentBindingSource.ResetBindings(false);
           konteragentTableAdapter.Update(storageDataSet.Konteragent);
            mgData.Refresh();
            TableEdited(false);
        }

        private void mlSave_Click(object sender, EventArgs e)
        {
            try
            {
                Edit(false);
                konteragentBindingSource.EndEdit();
                konteragentTableAdapter.Update(storageDataSet.Konteragent);
                mgData.Refresh();
                TableEdited(false);
                MessageBox.Show("Збережено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Konteragent.RejectChanges();
                //throw;
            }
        }

        private void Search()
        {
            if (!string.IsNullOrEmpty(txSearch.Text))
            {
                //get table name where we gonna search
                string dbTableName = mgData.Columns[cbSearch.SelectedIndex].DataPropertyName;
                //set filter to display in datagrid
                konteragentBindingSource.Filter = string.Format("{0} LIKE '*{1}*'", dbTableName, txSearch.Text);
            }
            else
            {
                konteragentBindingSource.Filter = string.Empty;
            }
        }

        //delete
        private void mgData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)//delete
            {
                if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    konteragentBindingSource.RemoveCurrent();

                }
            }
        }

        private void cbSearch_TextChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void mlSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void mlDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                konteragentBindingSource.RemoveCurrent();
                TableEdited(true);
            }
        }

        #endregion

        private void ButtonMouseLeave(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.Transparent;
        }

        private void ButtonMouseEnter(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.FromArgb(215, 215, 215);
        }

    }
}

﻿namespace ProbaCursApp
{
    partial class ucDashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDashboard));
            this.linkOnStorage = new System.Windows.Forms.LinkLabel();
            this.linkKonteragent = new System.Windows.Forms.LinkLabel();
            this.linkNadhod = new System.Windows.Forms.LinkLabel();
            this.lComment = new System.Windows.Forms.Label();
            this.bUnlogin = new System.Windows.Forms.Button();
            this.mlProduct = new MetroFramework.Controls.MetroLink();
            this.mlKonteragent = new MetroFramework.Controls.MetroLink();
            this.mlNadhod = new MetroFramework.Controls.MetroLink();
            this.mlStorage = new MetroFramework.Controls.MetroLink();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lDirectories = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.linkProduct = new System.Windows.Forms.LinkLabel();
            this.lQuery = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // linkOnStorage
            // 
            this.linkOnStorage.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkOnStorage.BackColor = System.Drawing.Color.White;
            this.linkOnStorage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkOnStorage.LinkColor = System.Drawing.Color.DimGray;
            this.linkOnStorage.Location = new System.Drawing.Point(9, 273);
            this.linkOnStorage.Name = "linkOnStorage";
            this.linkOnStorage.Size = new System.Drawing.Size(140, 70);
            this.linkOnStorage.TabIndex = 21;
            this.linkOnStorage.TabStop = true;
            this.linkOnStorage.Text = "Що на складі?";
            this.linkOnStorage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkOnStorage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkOnStorage_LinkClicked);
            this.linkOnStorage.MouseEnter += new System.EventHandler(this.TextEnter);
            this.linkOnStorage.MouseLeave += new System.EventHandler(this.TextLeave);
            // 
            // linkKonteragent
            // 
            this.linkKonteragent.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkKonteragent.BackColor = System.Drawing.Color.White;
            this.linkKonteragent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkKonteragent.LinkColor = System.Drawing.Color.DimGray;
            this.linkKonteragent.Location = new System.Drawing.Point(9, 198);
            this.linkKonteragent.Name = "linkKonteragent";
            this.linkKonteragent.Padding = new System.Windows.Forms.Padding(5);
            this.linkKonteragent.Size = new System.Drawing.Size(140, 70);
            this.linkKonteragent.TabIndex = 22;
            this.linkKonteragent.TabStop = true;
            this.linkKonteragent.Text = "Що поставляв?";
            this.linkKonteragent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkKonteragent.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkKonteragent_LinkClicked);
            this.linkKonteragent.MouseEnter += new System.EventHandler(this.TextEnter);
            this.linkKonteragent.MouseLeave += new System.EventHandler(this.TextLeave);
            // 
            // linkNadhod
            // 
            this.linkNadhod.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkNadhod.BackColor = System.Drawing.Color.White;
            this.linkNadhod.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkNadhod.LinkColor = System.Drawing.Color.DimGray;
            this.linkNadhod.Location = new System.Drawing.Point(9, 47);
            this.linkNadhod.Name = "linkNadhod";
            this.linkNadhod.Size = new System.Drawing.Size(140, 70);
            this.linkNadhod.TabIndex = 23;
            this.linkNadhod.TabStop = true;
            this.linkNadhod.Text = "Коли надійшло?";
            this.linkNadhod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkNadhod.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkNadhod_LinkClicked);
            this.linkNadhod.MouseEnter += new System.EventHandler(this.TextEnter);
            this.linkNadhod.MouseLeave += new System.EventHandler(this.TextLeave);
            // 
            // lComment
            // 
            this.lComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lComment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.lComment.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lComment.Location = new System.Drawing.Point(336, 14);
            this.lComment.Name = "lComment";
            this.lComment.Size = new System.Drawing.Size(359, 30);
            this.lComment.TabIndex = 24;
            this.lComment.Text = "Comment";
            this.lComment.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // bUnlogin
            // 
            this.bUnlogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bUnlogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bUnlogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bUnlogin.FlatAppearance.BorderSize = 0;
            this.bUnlogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bUnlogin.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bUnlogin.Image = ((System.Drawing.Image)(resources.GetObject("bUnlogin.Image")));
            this.bUnlogin.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bUnlogin.Location = new System.Drawing.Point(693, 0);
            this.bUnlogin.Name = "bUnlogin";
            this.bUnlogin.Size = new System.Drawing.Size(67, 44);
            this.bUnlogin.TabIndex = 28;
            this.bUnlogin.Text = "Вийти";
            this.bUnlogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bUnlogin.UseVisualStyleBackColor = false;
            this.bUnlogin.Click += new System.EventHandler(this.bUnlogin_Click);
            // 
            // mlProduct
            // 
            this.mlProduct.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlProduct.Image = global::ProbaCursApp.Properties.Resources.product;
            this.mlProduct.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlProduct.ImageSize = 50;
            this.mlProduct.Location = new System.Drawing.Point(28, 135);
            this.mlProduct.Name = "mlProduct";
            this.mlProduct.Size = new System.Drawing.Size(140, 70);
            this.mlProduct.TabIndex = 4;
            this.mlProduct.Text = "Товари";
            this.mlProduct.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlProduct.UseSelectable = true;
            this.mlProduct.Click += new System.EventHandler(this.mlProduct_Click);
            // 
            // mlKonteragent
            // 
            this.mlKonteragent.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlKonteragent.Image = global::ProbaCursApp.Properties.Resources.Konteagent;
            this.mlKonteragent.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlKonteragent.ImageSize = 50;
            this.mlKonteragent.Location = new System.Drawing.Point(28, 210);
            this.mlKonteragent.Name = "mlKonteragent";
            this.mlKonteragent.Size = new System.Drawing.Size(140, 70);
            this.mlKonteragent.TabIndex = 3;
            this.mlKonteragent.Text = "Контерагенти";
            this.mlKonteragent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlKonteragent.UseSelectable = true;
            this.mlKonteragent.Click += new System.EventHandler(this.mlKonteragent_Click);
            // 
            // mlNadhod
            // 
            this.mlNadhod.DisplayFocus = true;
            this.mlNadhod.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlNadhod.Image = global::ProbaCursApp.Properties.Resources.Nadhodh;
            this.mlNadhod.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlNadhod.ImageSize = 50;
            this.mlNadhod.Location = new System.Drawing.Point(28, 60);
            this.mlNadhod.Name = "mlNadhod";
            this.mlNadhod.Size = new System.Drawing.Size(140, 70);
            this.mlNadhod.TabIndex = 2;
            this.mlNadhod.Text = "Надходження";
            this.mlNadhod.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlNadhod.UseSelectable = true;
            this.mlNadhod.Click += new System.EventHandler(this.mlNadhod_Click);
            // 
            // mlStorage
            // 
            this.mlStorage.DisplayFocus = true;
            this.mlStorage.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.mlStorage.Image = global::ProbaCursApp.Properties.Resources.storage;
            this.mlStorage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlStorage.ImageSize = 50;
            this.mlStorage.Location = new System.Drawing.Point(28, 285);
            this.mlStorage.Name = "mlStorage";
            this.mlStorage.Size = new System.Drawing.Size(140, 70);
            this.mlStorage.TabIndex = 0;
            this.mlStorage.Text = "Склади";
            this.mlStorage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlStorage.UseSelectable = true;
            this.mlStorage.Click += new System.EventHandler(this.mlStorage_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.lDirectories);
            this.panel1.Location = new System.Drawing.Point(18, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(158, 350);
            this.panel1.TabIndex = 29;
            // 
            // lDirectories
            // 
            this.lDirectories.AutoSize = true;
            this.lDirectories.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lDirectories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lDirectories.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDirectories.Location = new System.Drawing.Point(27, 5);
            this.lDirectories.Name = "lDirectories";
            this.lDirectories.Size = new System.Drawing.Size(107, 25);
            this.lDirectories.TabIndex = 21;
            this.lDirectories.Text = "Довідники";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Controls.Add(this.linkProduct);
            this.panel2.Controls.Add(this.lQuery);
            this.panel2.Controls.Add(this.linkNadhod);
            this.panel2.Controls.Add(this.linkOnStorage);
            this.panel2.Controls.Add(this.linkKonteragent);
            this.panel2.Location = new System.Drawing.Point(182, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(158, 350);
            this.panel2.TabIndex = 30;
            // 
            // linkProduct
            // 
            this.linkProduct.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkProduct.BackColor = System.Drawing.Color.White;
            this.linkProduct.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkProduct.LinkColor = System.Drawing.Color.DimGray;
            this.linkProduct.Location = new System.Drawing.Point(9, 122);
            this.linkProduct.Name = "linkProduct";
            this.linkProduct.Size = new System.Drawing.Size(140, 70);
            this.linkProduct.TabIndex = 24;
            this.linkProduct.TabStop = true;
            this.linkProduct.Text = "Де товар?";
            this.linkProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkProduct.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkProduct_Click);
            // 
            // lQuery
            // 
            this.lQuery.AutoSize = true;
            this.lQuery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lQuery.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lQuery.Location = new System.Drawing.Point(30, 5);
            this.lQuery.Name = "lQuery";
            this.lQuery.Size = new System.Drawing.Size(73, 25);
            this.lQuery.TabIndex = 22;
            this.lQuery.Text = "Запити";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::ProbaCursApp.Properties.Resources.Storagbge;
            this.pictureBox1.Location = new System.Drawing.Point(-136, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(896, 520);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // ucDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.bUnlogin);
            this.Controls.Add(this.mlProduct);
            this.Controls.Add(this.mlKonteragent);
            this.Controls.Add(this.mlNadhod);
            this.Controls.Add(this.mlStorage);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lComment);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ucDashboard";
            this.Size = new System.Drawing.Size(760, 520);
            this.Load += new System.EventHandler(this.usDashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.LinkLabel linkOnStorage;
        public System.Windows.Forms.LinkLabel linkKonteragent;
        public System.Windows.Forms.LinkLabel linkNadhod;
        public System.Windows.Forms.Label lComment;
        public System.Windows.Forms.Button bUnlogin;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Label lDirectories;
        public System.Windows.Forms.Label lQuery;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.LinkLabel linkProduct;
        public MetroFramework.Controls.MetroLink mlStorage;
        public MetroFramework.Controls.MetroLink mlKonteragent;
        public MetroFramework.Controls.MetroLink mlNadhod;
        public MetroFramework.Controls.MetroLink mlProduct;
    }
}

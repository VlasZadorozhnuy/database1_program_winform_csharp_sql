﻿namespace ProbaCursApp
{
    partial class ucProduct
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.txVumiry = new MetroFramework.Controls.MetroTextBox();
            this.tovaruBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.storageDataSet = new ProbaCursApp.StorageDataSet();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txName = new MetroFramework.Controls.MetroTextBox();
            this.l1 = new MetroFramework.Controls.MetroLabel();
            this.txArtukyl = new MetroFramework.Controls.MetroTextBox();
            this.l0 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.cbSearch = new MetroFramework.Controls.MetroComboBox();
            this.txSearch = new MetroFramework.Controls.MetroTextBox();
            this.mlSearch = new MetroFramework.Controls.MetroLink();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.mlDelete = new MetroFramework.Controls.MetroLink();
            this.mlEdit = new MetroFramework.Controls.MetroLink();
            this.mlSave = new MetroFramework.Controls.MetroLink();
            this.mlCancel = new MetroFramework.Controls.MetroLink();
            this.mlNew = new MetroFramework.Controls.MetroLink();
            this.mgData = new MetroFramework.Controls.MetroGrid();
            this.artukylDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazvaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odVumiryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tovaruTableAdapter = new ProbaCursApp.StorageDataSetTableAdapters.TovaruTableAdapter();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tovaruBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storageDataSet)).BeginInit();
            this.metroPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel3
            // 
            this.metroPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel3.Controls.Add(this.txVumiry);
            this.metroPanel3.Controls.Add(this.metroLabel1);
            this.metroPanel3.Controls.Add(this.txName);
            this.metroPanel3.Controls.Add(this.l1);
            this.metroPanel3.Controls.Add(this.txArtukyl);
            this.metroPanel3.Controls.Add(this.l0);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(4, 3);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(367, 119);
            this.metroPanel3.TabIndex = 14;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // txVumiry
            // 
            // 
            // 
            // 
            this.txVumiry.CustomButton.Image = null;
            this.txVumiry.CustomButton.Location = new System.Drawing.Point(250, 1);
            this.txVumiry.CustomButton.Name = "";
            this.txVumiry.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txVumiry.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txVumiry.CustomButton.TabIndex = 1;
            this.txVumiry.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txVumiry.CustomButton.UseSelectable = true;
            this.txVumiry.CustomButton.Visible = false;
            this.txVumiry.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tovaruBindingSource, "OdVumiry", true));
            this.txVumiry.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txVumiry.Lines = new string[] {
        "txVumiry"};
            this.txVumiry.Location = new System.Drawing.Point(90, 65);
            this.txVumiry.MaxLength = 32767;
            this.txVumiry.Name = "txVumiry";
            this.txVumiry.PasswordChar = '\0';
            this.txVumiry.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txVumiry.SelectedText = "";
            this.txVumiry.SelectionLength = 0;
            this.txVumiry.SelectionStart = 0;
            this.txVumiry.ShortcutsEnabled = true;
            this.txVumiry.Size = new System.Drawing.Size(274, 25);
            this.txVumiry.Style = MetroFramework.MetroColorStyle.Silver;
            this.txVumiry.TabIndex = 12;
            this.txVumiry.Text = "txVumiry";
            this.txVumiry.UseSelectable = true;
            this.txVumiry.UseStyleColors = true;
            this.txVumiry.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txVumiry.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tovaruBindingSource
            // 
            this.tovaruBindingSource.DataMember = "Tovaru";
            this.tovaruBindingSource.DataSource = this.storageDataSet;
            // 
            // storageDataSet
            // 
            this.storageDataSet.DataSetName = "StorageDataSet";
            this.storageDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 69);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(75, 19);
            this.metroLabel1.TabIndex = 13;
            this.metroLabel1.Text = "Од. виміру";
            // 
            // txName
            // 
            // 
            // 
            // 
            this.txName.CustomButton.Image = null;
            this.txName.CustomButton.Location = new System.Drawing.Point(250, 1);
            this.txName.CustomButton.Name = "";
            this.txName.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txName.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txName.CustomButton.TabIndex = 1;
            this.txName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txName.CustomButton.UseSelectable = true;
            this.txName.CustomButton.Visible = false;
            this.txName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tovaruBindingSource, "Nazva", true));
            this.txName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txName.Lines = new string[] {
        "txName"};
            this.txName.Location = new System.Drawing.Point(90, 34);
            this.txName.MaxLength = 32767;
            this.txName.Name = "txName";
            this.txName.PasswordChar = '\0';
            this.txName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txName.SelectedText = "";
            this.txName.SelectionLength = 0;
            this.txName.SelectionStart = 0;
            this.txName.ShortcutsEnabled = true;
            this.txName.Size = new System.Drawing.Size(274, 25);
            this.txName.Style = MetroFramework.MetroColorStyle.Silver;
            this.txName.TabIndex = 9;
            this.txName.Text = "txName";
            this.txName.UseSelectable = true;
            this.txName.UseStyleColors = true;
            this.txName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Location = new System.Drawing.Point(3, 38);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(45, 19);
            this.l1.TabIndex = 11;
            this.l1.Text = "Назва";
            // 
            // txArtukyl
            // 
            // 
            // 
            // 
            this.txArtukyl.CustomButton.Image = null;
            this.txArtukyl.CustomButton.Location = new System.Drawing.Point(250, 1);
            this.txArtukyl.CustomButton.Name = "";
            this.txArtukyl.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txArtukyl.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txArtukyl.CustomButton.TabIndex = 1;
            this.txArtukyl.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txArtukyl.CustomButton.UseSelectable = true;
            this.txArtukyl.CustomButton.Visible = false;
            this.txArtukyl.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tovaruBindingSource, "Artukyl", true));
            this.txArtukyl.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txArtukyl.Lines = new string[] {
        "txArtukyl"};
            this.txArtukyl.Location = new System.Drawing.Point(90, 5);
            this.txArtukyl.MaxLength = 32767;
            this.txArtukyl.Name = "txArtukyl";
            this.txArtukyl.PasswordChar = '\0';
            this.txArtukyl.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txArtukyl.SelectedText = "";
            this.txArtukyl.SelectionLength = 0;
            this.txArtukyl.SelectionStart = 0;
            this.txArtukyl.ShortcutsEnabled = true;
            this.txArtukyl.Size = new System.Drawing.Size(274, 25);
            this.txArtukyl.Style = MetroFramework.MetroColorStyle.Silver;
            this.txArtukyl.TabIndex = 8;
            this.txArtukyl.Text = "txArtukyl";
            this.txArtukyl.UseSelectable = true;
            this.txArtukyl.UseStyleColors = true;
            this.txArtukyl.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txArtukyl.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // l0
            // 
            this.l0.AutoSize = true;
            this.l0.Location = new System.Drawing.Point(3, 9);
            this.l0.Name = "l0";
            this.l0.Size = new System.Drawing.Size(57, 19);
            this.l0.TabIndex = 10;
            this.l0.Text = "Артикул";
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel2.Controls.Add(this.cbSearch);
            this.metroPanel2.Controls.Add(this.txSearch);
            this.metroPanel2.Controls.Add(this.mlSearch);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(407, 128);
            this.metroPanel2.Margin = new System.Windows.Forms.Padding(1);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(353, 40);
            this.metroPanel2.TabIndex = 13;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // cbSearch
            // 
            this.cbSearch.FormattingEnabled = true;
            this.cbSearch.ItemHeight = 23;
            this.cbSearch.Location = new System.Drawing.Point(223, 8);
            this.cbSearch.Name = "cbSearch";
            this.cbSearch.Size = new System.Drawing.Size(121, 29);
            this.cbSearch.TabIndex = 12;
            this.cbSearch.UseSelectable = true;
            this.cbSearch.TextChanged += new System.EventHandler(this.cbSearch_TextChanged);
            // 
            // txSearch
            // 
            // 
            // 
            // 
            this.txSearch.CustomButton.Image = null;
            this.txSearch.CustomButton.Location = new System.Drawing.Point(116, 1);
            this.txSearch.CustomButton.Name = "";
            this.txSearch.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txSearch.CustomButton.TabIndex = 1;
            this.txSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txSearch.CustomButton.UseSelectable = true;
            this.txSearch.CustomButton.Visible = false;
            this.txSearch.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txSearch.Lines = new string[0];
            this.txSearch.Location = new System.Drawing.Point(73, 8);
            this.txSearch.MaxLength = 32767;
            this.txSearch.Name = "txSearch";
            this.txSearch.PasswordChar = '\0';
            this.txSearch.PromptText = "Що шукати?..";
            this.txSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txSearch.SelectedText = "";
            this.txSearch.SelectionLength = 0;
            this.txSearch.SelectionStart = 0;
            this.txSearch.ShortcutsEnabled = true;
            this.txSearch.Size = new System.Drawing.Size(144, 29);
            this.txSearch.TabIndex = 11;
            this.txSearch.UseSelectable = true;
            this.txSearch.WaterMark = "Що шукати?..";
            this.txSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txSearch.TextChanged += new System.EventHandler(this.txSearch_TextChanged);
            // 
            // mlSearch
            // 
            this.mlSearch.BackColor = System.Drawing.Color.Transparent;
            this.mlSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlSearch.Image = global::ProbaCursApp.Properties.Resources.search;
            this.mlSearch.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlSearch.ImageSize = 25;
            this.mlSearch.Location = new System.Drawing.Point(0, 0);
            this.mlSearch.Margin = new System.Windows.Forms.Padding(0);
            this.mlSearch.Name = "mlSearch";
            this.mlSearch.Size = new System.Drawing.Size(70, 40);
            this.mlSearch.TabIndex = 7;
            this.mlSearch.Text = "Пошук";
            this.mlSearch.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlSearch.UseCustomBackColor = true;
            this.mlSearch.UseSelectable = true;
            this.mlSearch.UseVisualStyleBackColor = false;
            this.mlSearch.Click += new System.EventHandler(this.mlSearch_Click);
            this.mlSearch.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlSearch.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel1.Controls.Add(this.mlDelete);
            this.metroPanel1.Controls.Add(this.mlEdit);
            this.metroPanel1.Controls.Add(this.mlSave);
            this.metroPanel1.Controls.Add(this.mlCancel);
            this.metroPanel1.Controls.Add(this.mlNew);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(4, 128);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(367, 40);
            this.metroPanel1.TabIndex = 12;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // mlDelete
            // 
            this.mlDelete.BackColor = System.Drawing.Color.Transparent;
            this.mlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlDelete.Image = global::ProbaCursApp.Properties.Resources.delete;
            this.mlDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlDelete.ImageSize = 25;
            this.mlDelete.Location = new System.Drawing.Point(294, 0);
            this.mlDelete.Name = "mlDelete";
            this.mlDelete.Size = new System.Drawing.Size(70, 40);
            this.mlDelete.TabIndex = 5;
            this.mlDelete.Text = "Видалити";
            this.mlDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlDelete.UseCustomBackColor = true;
            this.mlDelete.UseSelectable = true;
            this.mlDelete.UseVisualStyleBackColor = false;
            this.mlDelete.Click += new System.EventHandler(this.mlDelete_Click);
            this.mlDelete.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlDelete.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlEdit
            // 
            this.mlEdit.BackColor = System.Drawing.Color.Transparent;
            this.mlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlEdit.Image = global::ProbaCursApp.Properties.Resources.edit;
            this.mlEdit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlEdit.ImageSize = 25;
            this.mlEdit.Location = new System.Drawing.Point(76, 0);
            this.mlEdit.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.mlEdit.Name = "mlEdit";
            this.mlEdit.Size = new System.Drawing.Size(71, 40);
            this.mlEdit.TabIndex = 4;
            this.mlEdit.Text = "Редагувати";
            this.mlEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlEdit.UseCustomBackColor = true;
            this.mlEdit.UseSelectable = true;
            this.mlEdit.UseVisualStyleBackColor = false;
            this.mlEdit.Click += new System.EventHandler(this.mlEdit_Click);
            this.mlEdit.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlEdit.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlSave
            // 
            this.mlSave.BackColor = System.Drawing.Color.Transparent;
            this.mlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlSave.Image = global::ProbaCursApp.Properties.Resources.save;
            this.mlSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlSave.ImageSize = 25;
            this.mlSave.Location = new System.Drawing.Point(225, 0);
            this.mlSave.Name = "mlSave";
            this.mlSave.Size = new System.Drawing.Size(70, 40);
            this.mlSave.TabIndex = 3;
            this.mlSave.Text = "Зберегти";
            this.mlSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlSave.UseCustomBackColor = true;
            this.mlSave.UseSelectable = true;
            this.mlSave.UseVisualStyleBackColor = false;
            this.mlSave.Click += new System.EventHandler(this.mlSave_Click);
            this.mlSave.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlSave.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlCancel
            // 
            this.mlCancel.BackColor = System.Drawing.Color.Transparent;
            this.mlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlCancel.Enabled = false;
            this.mlCancel.Image = global::ProbaCursApp.Properties.Resources.cancel;
            this.mlCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlCancel.ImageSize = 25;
            this.mlCancel.Location = new System.Drawing.Point(150, 0);
            this.mlCancel.Name = "mlCancel";
            this.mlCancel.Size = new System.Drawing.Size(70, 40);
            this.mlCancel.TabIndex = 2;
            this.mlCancel.Text = "Відміна";
            this.mlCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlCancel.UseCustomBackColor = true;
            this.mlCancel.UseSelectable = true;
            this.mlCancel.UseVisualStyleBackColor = false;
            this.mlCancel.Click += new System.EventHandler(this.mlCancel_Click);
            this.mlCancel.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlCancel.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlNew
            // 
            this.mlNew.BackColor = System.Drawing.Color.Transparent;
            this.mlNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlNew.Image = global::ProbaCursApp.Properties.Resources.ico_new;
            this.mlNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlNew.ImageSize = 25;
            this.mlNew.Location = new System.Drawing.Point(0, 0);
            this.mlNew.Margin = new System.Windows.Forms.Padding(0);
            this.mlNew.Name = "mlNew";
            this.mlNew.Size = new System.Drawing.Size(70, 40);
            this.mlNew.TabIndex = 0;
            this.mlNew.Text = "Новий";
            this.mlNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlNew.UseCustomBackColor = true;
            this.mlNew.UseSelectable = true;
            this.mlNew.UseVisualStyleBackColor = false;
            this.mlNew.Click += new System.EventHandler(this.mlNew_Click);
            this.mlNew.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlNew.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mgData
            // 
            this.mgData.AllowUserToResizeRows = false;
            this.mgData.AutoGenerateColumns = false;
            this.mgData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mgData.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mgData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.mgData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mgData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artukylDataGridViewTextBoxColumn,
            this.nazvaDataGridViewTextBoxColumn,
            this.odVumiryDataGridViewTextBoxColumn});
            this.mgData.DataSource = this.tovaruBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mgData.DefaultCellStyle = dataGridViewCellStyle2;
            this.mgData.EnableHeadersVisualStyles = false;
            this.mgData.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.mgData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.ImeMode = System.Windows.Forms.ImeMode.On;
            this.mgData.Location = new System.Drawing.Point(4, 169);
            this.mgData.MultiSelect = false;
            this.mgData.Name = "mgData";
            this.mgData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.mgData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.mgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mgData.Size = new System.Drawing.Size(754, 349);
            this.mgData.Style = MetroFramework.MetroColorStyle.Silver;
            this.mgData.TabIndex = 11;
            this.mgData.UseCustomBackColor = true;
            this.mgData.UseCustomForeColor = true;
            this.mgData.UseStyleColors = true;
            this.mgData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mgData_KeyDown);
            // 
            // artukylDataGridViewTextBoxColumn
            // 
            this.artukylDataGridViewTextBoxColumn.DataPropertyName = "Artukyl";
            this.artukylDataGridViewTextBoxColumn.HeaderText = "Artukyl";
            this.artukylDataGridViewTextBoxColumn.Name = "artukylDataGridViewTextBoxColumn";
            // 
            // nazvaDataGridViewTextBoxColumn
            // 
            this.nazvaDataGridViewTextBoxColumn.DataPropertyName = "Nazva";
            this.nazvaDataGridViewTextBoxColumn.HeaderText = "Nazva";
            this.nazvaDataGridViewTextBoxColumn.Name = "nazvaDataGridViewTextBoxColumn";
            // 
            // odVumiryDataGridViewTextBoxColumn
            // 
            this.odVumiryDataGridViewTextBoxColumn.DataPropertyName = "OdVumiry";
            this.odVumiryDataGridViewTextBoxColumn.HeaderText = "OdVumiry";
            this.odVumiryDataGridViewTextBoxColumn.Name = "odVumiryDataGridViewTextBoxColumn";
            // 
            // tovaruTableAdapter
            // 
            this.tovaruTableAdapter.ClearBeforeFill = true;
            // 
            // ucProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.mgData);
            this.Name = "ucProduct";
            this.Size = new System.Drawing.Size(760, 520);
            this.Load += new System.EventHandler(this.ucProduct_Load);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tovaruBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storageDataSet)).EndInit();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroComboBox cbSearch;
        private MetroFramework.Controls.MetroTextBox txSearch;
        private MetroFramework.Controls.MetroLink mlSearch;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLink mlDelete;
        private MetroFramework.Controls.MetroLink mlEdit;
        private MetroFramework.Controls.MetroLink mlSave;
        private MetroFramework.Controls.MetroLink mlCancel;
        private MetroFramework.Controls.MetroLink mlNew;
        private MetroFramework.Controls.MetroGrid mgData;
        private MetroFramework.Controls.MetroTextBox txName;
        private MetroFramework.Controls.MetroLabel l1;
        private MetroFramework.Controls.MetroTextBox txArtukyl;
        private MetroFramework.Controls.MetroLabel l0;
        private MetroFramework.Controls.MetroTextBox txVumiry;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.BindingSource tovaruBindingSource;
        private StorageDataSet storageDataSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn artukylDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazvaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn odVumiryDataGridViewTextBoxColumn;
        private StorageDataSetTableAdapters.TovaruTableAdapter tovaruTableAdapter;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
    public partial class Report : Form
    {
        SqlConnection connection;
        string connectionString;

        public Report()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        public void LoadStorageContainReport(string parametrIdSklad)
        {
            string query = "SELECT Nadhod.Artukyl, Tovaru.Nazva, SUM(Nadhod.Kilkist), Sklad.MVO, Sklad.NazvaSkladu FROM Nadhod " +
                "INNER JOIN Tovaru ON Nadhod.Artukyl = Tovaru.Artukyl "+
                "INNER JOIN Sklad ON Nadhod.IdSklad = Sklad.Id " +
                "WHERE(Nadhod.IdSklad = @idSkladParameter) GROUP BY Nadhod.Artukyl, Tovaru.Nazva, Sklad.MVO , NazvaSkladu";
            using (connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        command.Parameters.AddWithValue("idSkladParameter", parametrIdSklad);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        var cry = new rptStorageContaine();

                        cry.SetDataSource(table);
                        //parametrs
                        //Name
                        cry.SetParameterValue(0, table.Rows[0][4].ToString());
                        //MVO
                        cry.SetParameterValue(1, table.Rows[0][3].ToString());
                        
                        //
                        crystalReportViewer1.ReportSource = cry;
                        crystalReportViewer1.Refresh();
                    }
                }
            }
        }

        public void LoadNadhod(DateTime date0, DateTime date1)
        {
            string query = "SELECT Nadhod.Date, Konteragent.Nazva, Nadhod.Artukyl, Tovaru.Nazva, Sklad.NazvaSkladu, Nadhod.Kilkist, Nadhod.CzinaZaOd" +
                       " FROM Nadhod" +
                       " INNER JOIN Tovaru ON Nadhod.Artukyl = Tovaru.Artukyl" +
                       " INNER JOIN Konteragent ON Nadhod.IdKonteragent = Konteragent.Id " +
                       " INNER JOIN Sklad ON Nadhod.IdSklad = Sklad.Id " +
            " WHERE (Nadhod.Date BETWEEN @date0 AND @date1 ) ";

            using (connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        command.Parameters.AddWithValue("date0", date0.ToString("yyyy.MM.dd"));
                        command.Parameters.AddWithValue("date1", date1.ToString("yyyy.MM.dd"));
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        var cry = new rptNadhod();
                        
                        cry.SetDataSource(table);
                        //parametrs
                        cry.SetParameterValue(0, date0.ToString("dd.MM.yyyy"));
                        cry.SetParameterValue(1, date1.ToString("dd.MM.yyyy"));
                        //
                        crystalReportViewer1.ReportSource = cry;
                        crystalReportViewer1.Refresh();
                    }
                }
            }
        }

        private void Report_Load(object sender, EventArgs e)
        {

        }
    }
}
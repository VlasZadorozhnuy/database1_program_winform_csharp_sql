﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace ProbaCursApp
{
    public partial class ucProduct : UserControl
    {

        #region Init
        public ucProduct()
        {
            InitializeComponent();
        }


        private void ucProduct_Load(object sender, EventArgs e)
        {
            
            fmMain.Instance.Text = "        Товари";
            fmMain.Instance.Refresh();
            this.tovaruTableAdapter.Fill(this.storageDataSet.Tovaru);
            Edit(false);
            //new headers text in grid
            ReplaceHeaders();
            //add headers values in combobox Search
            AddComboboxItems();
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
        }

        bool tableEdited = false;

        private void ReplaceHeaders()
        {
            mgData.Columns[0].HeaderText = "Артикул";
            mgData.Columns[1].HeaderText = "Назва";
            mgData.Columns[2].HeaderText = "Од.виміру";
        }

        private void AddComboboxItems()
        {
            for (int i = 0; i < mgData.Columns.Count; i++)
            {
                cbSearch.Items.Add(mgData.Columns[i].HeaderText);
            }
            cbSearch.SelectedIndex = 0;
        }    

        #endregion
        #region navigation logic

        private void Edit(bool value)
        {
            txName.Enabled = value;
            txArtukyl.Enabled = value;
            txVumiry.Enabled = value;
            mlCancel.Enabled = true;
        }

        private void TableEdited(bool value)
        {
            mlCancel.Enabled = value;
            tableEdited = value;
            fmMain.Instance.LAsterix.Visible= value;
        }

        private void mlNew_Click(object sender, EventArgs e)
        {
            try
            {

                Edit(true);
                storageDataSet.Tovaru.AddTovaruRow(storageDataSet.Tovaru.NewTovaruRow());
                tovaruBindingSource.MoveLast();
                txName.Focus();
                TableEdited(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Tovaru.RejectChanges();
                //throw;
            }
        }

        private void mlEdit_Click(object sender, EventArgs e)
        {
            Edit(true);
            txArtukyl.Focus();
            TableEdited(true);
        }

        private void mlCancel_Click(object sender, EventArgs e)
        {
            Edit(false);
            tovaruBindingSource.CancelEdit();
            tovaruBindingSource.ResetBindings(false);
            tovaruTableAdapter.Update(storageDataSet.Tovaru);
            mgData.Refresh();
            TableEdited(false);
        }

        private void mlSave_Click(object sender, EventArgs e)
        {
            try
            {
                Edit(false);
                tovaruBindingSource.EndEdit();
                tovaruTableAdapter.Update(storageDataSet.Tovaru);
                mgData.Refresh();
                TableEdited(false);
                MessageBox.Show("Збережено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Tovaru.RejectChanges();
                //throw;
            }
        }

        private void Search()
        {
            if (!string.IsNullOrEmpty(txSearch.Text))
            {
                //get table name where we gonna search
                string dbTableName = mgData.Columns[cbSearch.SelectedIndex].DataPropertyName;
                //set filter to display in datagrid
                tovaruBindingSource.Filter = string.Format("{0} LIKE '*{1}*'", dbTableName, txSearch.Text);
            }
            else
            {
                tovaruBindingSource.Filter = string.Empty;
            }
        }

        private void txSearch_TextChanged(object sender, EventArgs e)
        {
            Search();
        }
        //delete
        private void mgData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)//delete
            {
                if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    tovaruBindingSource.RemoveCurrent();
                    TableEdited(true);
                }
            }
        }

        private void cbSearch_TextChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void mlSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void mlDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                tovaruBindingSource.RemoveCurrent();
                TableEdited(true);
            }
        }

        #endregion

        private void ButtonMouseLeave(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.Transparent;
        }

        private void ButtonMouseEnter(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.FromArgb(215, 215, 215);
        }

    }
}

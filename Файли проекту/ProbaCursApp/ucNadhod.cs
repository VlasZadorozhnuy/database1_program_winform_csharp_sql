﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
   // public partial class ucNadhod : MetroFramework.Controls.MetroUserControl;
    public partial class ucNadhod : MetroFramework.Controls.MetroUserControl
    {
        bool tableEdited=false;
        SqlConnection connection;
        string connectionString;

        #region Init

        private void ReplaceHeaders() {
            mgData.Columns[0].HeaderText = "Дата";
            mgData.Columns[1].HeaderText = "Склад";
            mgData.Columns[2].HeaderText = "Контерагент";
            mgData.Columns[3].HeaderText = "Артикул";
            mgData.Columns[4].HeaderText = "Кількість";
            mgData.Columns[5].HeaderText = "Ціна за од.";
        }

      

        public ucNadhod()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        private void ucNadhod_Load(object sender, EventArgs e)
        {
            
            fmMain.Instance.Text = "        Надходження";
            fmMain.Instance.Refresh();
            ComboTovaruRefresh();

            this.nadhodTableAdapter.Fill(this.storageDataSet.Nadhod);
            // TODO: This line of code loads data into the 'storageDataSet.Konteragent' table. You can move, or remove it, as needed.
            this.konteragentTableAdapter.Fill(this.storageDataSet.Konteragent);
            this.skladTableAdapter.Fill(this.storageDataSet.Sklad);
            this.tovaruTableAdapter.Fill(this.storageDataSet.Tovaru);

            Edit(false);
            //new headers text in grid
            ReplaceHeaders();
            //add headers values in combobox Search
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;

        }
        #endregion
        #region navigation logic

        private void Edit(bool value) {
            //txDate.Enabled = value;
            cbName.Enabled = value;
            cbStorage.Enabled = value;
            cbKonteragent.Enabled = value;
            txCount.Enabled = value;
            txPrice.Enabled = value;
            dateDateTimePicker.Enabled = value;
            mlCancel.Enabled = true;
        }

        private void TableEdited(bool value)
        {
            mlCancel.Enabled = value;
            tableEdited = value;
            fmMain.Instance.LAsterix.Visible = value;
        }

        private void ComboTovaruRefresh() {
            tovaruBindingSource.EndEdit();
            tovaruBindingSource.ResetBindings(true);
            tovaruTableAdapter.Fill(this.storageDataSet.Tovaru);
            cbName.DataSource = null;
            cbName.Items.Clear();
            cbName.DisplayMember = "Nazva";
            cbName.ValueMember= "Artukyl";
            cbName.SelectedValue = "nadhodBindingSource - Artukyl";
            
            cbName.DataSource = tovaruBindingSource;
        }

        private void mlNew_Click(object sender, EventArgs e)
        {
            try
            {
                
                Edit(true);
                storageDataSet.Nadhod.AddNadhodRow(storageDataSet.Nadhod.NewNadhodRow());
                nadhodBindingSource.MoveLast();
                //txDate.Focus();
                TableEdited(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Nadhod.RejectChanges();
                //throw;
            }
        }

        private void mlEdit_Click(object sender, EventArgs e)
        {
            Edit(true);
            //txDate.Focus();
            TableEdited(true);
        }

        private void mlCancel_Click(object sender, EventArgs e)
        {
            Edit(false);
            nadhodBindingSource.CancelEdit();
            nadhodBindingSource.ResetBindings(false);
            nadhodTableAdapter.Update(storageDataSet.Nadhod);
            mgData.Refresh();
            TableEdited(false);
        }

        private void mlSave_Click(object sender, EventArgs e)
        {
            try
            {
                Edit(false);
                this.Validate();
                this.nadhodBindingSource.EndEdit();
                this.nadhodTableAdapter.Update(this.storageDataSet);
                mgData.Refresh();
                TableEdited(false);
                MessageBox.Show("Збережено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Nadhod.RejectChanges();
               // throw;
            }
        }
        
        //delete
        private void mgData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)//delete
            {
                if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    nadhodBindingSource.RemoveCurrent();

                }
            }
        }

       
        private void mlDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                nadhodBindingSource.RemoveCurrent();
                TableEdited(true);
            }
        }

        #endregion

        private void ButtonMouseLeave(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.Transparent;
        }

        private void ButtonMouseEnter(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.FromArgb(215, 215, 215);
        }

        private void Changes(object sender, EventArgs e)
        {
            mgData.Refresh();
        }

        private void linkNewProduct_Click(object sender, LinkLabelLinkClickedEventArgs e)
        {
            mpNewProduct.Visible = true;
        }

        private void bSaveProduct_Click(object sender, EventArgs e)
        {
            string query = "INSERT INTO [dbo].[Tovaru] ([Artukyl], [Nazva], [OdVumiry]) VALUES (@Artukyl, @Nazva, @OdVumiry)";
            //string query = "SAVE "+txArtukyl.Text+","+txName + "," + txVumiry+" TO Tovaru";

            using (connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        connection.Open();
                        //impotant
                        command.Parameters.AddWithValue("Artukyl", txArtukyl.Text);
                        command.Parameters.AddWithValue("Nazva", txName.Text);
                        command.Parameters.AddWithValue("OdVumiry", txVumiry.Text);
                        try
                        {
                            command.ExecuteNonQuery();
                            MessageBox.Show("Збережено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            mpNewProduct.Visible = false;
                            ComboTovaruRefresh();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                           // throw;
                        }
                        connection.Close();
                    }

                }
            }
        }

        private void bProductCancel_Click(object sender, EventArgs e)
        {
            mpNewProduct.Visible = false;
        }
    }
}

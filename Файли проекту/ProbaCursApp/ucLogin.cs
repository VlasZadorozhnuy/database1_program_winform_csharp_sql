﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
    public partial class ucLogin : UserControl
    {
        SqlConnection connection;
        string connectionString;
        //
        string superLogin = "";
        string superPassword = "";

        int permission = -1;
        string comment = "";

        static ucLogin _instance;
        public static ucLogin Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ucLogin();
                return _instance;
            }
        }

        public int Permission
        {
            get { return permission; }
        }

        public string Comment
        {
            get { return comment; }
        }


        public ucLogin()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
        //check superUser
            if (txLogin.Text == superLogin)
            {
                if (txPassword.Text == superPassword)
                {
                    fmMain.Instance.LoadDashboard(1,"root");
                    return;
                }
            }
            //else check in database
            if (Logining()) {
                fmMain.Instance.LoadDashboard(permission, comment);
                return;
            }

            //else
            lWarning.Text = "Логін або пароль неправильний.\n" +
                "Якщо ви забули пароль, звернітся до адміністратора.";
        }

        

        private bool Logining() {
            string query = "SELECT Id, Login, Password, Permission, Сomment FROM [User] WHERE ( Login = @loginParam AND Password = @passParam )";

            using (connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        //connection.Open();
                        //impotant
                        command.Parameters.AddWithValue("loginParam", txLogin.Text);
                        command.Parameters.AddWithValue("passParam", txPassword.Text);
                        try
                        {
                           // command.ExecuteNonQuery();
                            DataTable table = new DataTable();
                            adapter.Fill(table);
                            //check exist user
                            if (table.Rows.Count <= 0)
                            {
                                //no users
                                return false;
                            }
                            else {
                                //get data permission, notes
                                permission = Int32.Parse(table.Rows[0]["Permission"].ToString());
                                //4=comment
                                comment = table.Rows[0][4].ToString();
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                            // throw;
                        }
                        //connection.Close();

                    }

                }
            }
            return true;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProbaCursApp
{
 //  public partial class ucDashboard : MetroFramework.Controls.MetroUserControl
    public partial class ucDashboard : MetroFramework.Controls.MetroUserControl
    {
        public ucDashboard()
        {
            InitializeComponent();
        }

        private void usDashboard_Load(object sender, EventArgs e)
        {
            fmMain.Instance.Text = "        Управління складами";
        }

        private void mlStorage_Click(object sender, EventArgs e)
        {
            ucStorage uc = new ucStorage();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("ucStorage"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("ucStorage");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["ucStorage"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void mlProduct_Click(object sender, EventArgs e)
        {
            ucProduct uc = new ucProduct();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("ucProduct"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("ucProduct");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["ucProduct"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void mlKonteragent_Click(object sender, EventArgs e)
        {
            ucKonteragent uc = new ucKonteragent();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("ucKonteragent"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("ucKonteragent");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["ucKonteragent"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void mlNadhod_Click(object sender, EventArgs e)
        {
            ucNadhod uc = new ucNadhod();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("ucNadhod"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("ucNadhod");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["ucNadhod"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void linkProduct_Click(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProductOnStorage uc = new ProductOnStorage();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("ProductOnStorage"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("ProductOnStorage");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["ProductOnStorage"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void linkOnStorage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            StorageContain uc = new StorageContain();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("StorageContain"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("StorageContain");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["StorageContain"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void linkKonteragent_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            KonteragentQuery uc = new KonteragentQuery();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("KonteragentQuery"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("KonteragentQuery");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["KonteragentQuery"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
            
        }

        private void linkNadhod_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NadhodQuery uc = new NadhodQuery();
            if (fmMain.Instance.MetroContainer.Controls.ContainsKey("NadhodQuery"))
            {
                fmMain.Instance.MetroContainer.Controls.RemoveByKey("NadhodQuery");
            }

            uc.Dock = DockStyle.Fill;
            fmMain.Instance.MetroContainer.Controls.Add(uc);

            fmMain.Instance.MetroContainer.Controls["NadhodQuery"].BringToFront();
            fmMain.Instance.MetroBack.Visible = true;
        }

        private void bUnlogin_Click(object sender, EventArgs e)
        {
            fmMain.Instance.LoadUcLogin();
        }

        private void TextEnter(object sender, EventArgs e)
        {
            var mt = sender as LinkLabel;
            mt.LinkColor = Color.Black;
        }

        private void TextLeave(object sender, EventArgs e)
        {
            var mt = sender as LinkLabel;
            mt.LinkColor = System.Drawing.Color.DimGray;
        }
    }
}

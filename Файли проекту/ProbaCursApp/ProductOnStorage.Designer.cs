﻿namespace ProbaCursApp
{
    partial class ProductOnStorage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cbProduct = new MetroFramework.Controls.MetroComboBox();
            this.mProduct = new MetroFramework.Controls.MetroLabel();
            this.mgData = new MetroFramework.Controls.MetroGrid();
            this.lProduct = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).BeginInit();
            this.SuspendLayout();
            // 
            // cbProduct
            // 
            this.cbProduct.DisplayMember = "Artukyl";
            this.cbProduct.FormattingEnabled = true;
            this.cbProduct.ItemHeight = 23;
            this.cbProduct.Location = new System.Drawing.Point(142, 21);
            this.cbProduct.Name = "cbProduct";
            this.cbProduct.Size = new System.Drawing.Size(200, 29);
            this.cbProduct.Style = MetroFramework.MetroColorStyle.Silver;
            this.cbProduct.TabIndex = 18;
            this.cbProduct.UseSelectable = true;
            this.cbProduct.ValueMember = "Artukyl";
            this.cbProduct.SelectedIndexChanged += new System.EventHandler(this.cbProduct_SelectedIndexChanged);
            // 
            // mProduct
            // 
            this.mProduct.AutoSize = true;
            this.mProduct.Location = new System.Drawing.Point(15, 20);
            this.mProduct.Name = "mProduct";
            this.mProduct.Size = new System.Drawing.Size(100, 23);
            this.mProduct.TabIndex = 0;
            // 
            // mgData
            // 
            this.mgData.AllowUserToAddRows = false;
            this.mgData.AllowUserToDeleteRows = false;
            this.mgData.AllowUserToResizeRows = false;
            this.mgData.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mgData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.mgData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.mgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mgData.DefaultCellStyle = dataGridViewCellStyle5;
            this.mgData.EnableHeadersVisualStyles = false;
            this.mgData.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.mgData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.Location = new System.Drawing.Point(22, 67);
            this.mgData.Name = "mgData";
            this.mgData.ReadOnly = true;
            this.mgData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.mgData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.mgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mgData.Size = new System.Drawing.Size(695, 238);
            this.mgData.Style = MetroFramework.MetroColorStyle.Silver;
            this.mgData.TabIndex = 19;
            // 
            // lProduct
            // 
            this.lProduct.AutoSize = true;
            this.lProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lProduct.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lProduct.Location = new System.Drawing.Point(18, 21);
            this.lProduct.Name = "lProduct";
            this.lProduct.Size = new System.Drawing.Size(118, 21);
            this.lProduct.TabIndex = 20;
            this.lProduct.Text = "Виберіть товар";
            // 
            // ProductOnStorage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lProduct);
            this.Controls.Add(this.mgData);
            this.Controls.Add(this.cbProduct);
            this.Name = "ProductOnStorage";
            this.Size = new System.Drawing.Size(760, 520);
            this.Load += new System.EventHandler(this.ProductOnStorage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cbProduct;
        private MetroFramework.Controls.MetroLabel mProduct;
        private MetroFramework.Controls.MetroGrid mgData;
        private System.Windows.Forms.Label lProduct;
    }
}

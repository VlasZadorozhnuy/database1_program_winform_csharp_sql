﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
    public partial class KonteragentQuery : UserControl
    {
        SqlConnection connection;
        string connectionString;
        public KonteragentQuery()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        private void KonteragentQuery_Load(object sender, EventArgs e)
        {
            
            fmMain.Instance.Text = "        Поставки";
            fmMain.Instance.Refresh();
            PopulateKonteragent();
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
        }

        private void PopulateKonteragent() {
            using (connection = new SqlConnection(connectionString) ) {
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Konteragent", connection)) {
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    cbProduct.DisplayMember = "Nazva";
                    cbProduct.ValueMember = "Id";
                    cbProduct.DataSource = table;
                    cbProduct.SelectedIndex = 0;
                }

            }
        }

        private void ReplaceHeaders()
        {
            mgData.Columns[0].HeaderText = "Дата";
            mgData.Columns[1].HeaderText = "Артикул";
            mgData.Columns[2].HeaderText = "Назва";
            mgData.Columns[3].HeaderText = "Кількість";
        }

        private void PopulateQuery()
        {
            
            string query = "SELECT Nadhod.Date, Nadhod.Artukyl, Tovaru.Nazva, Nadhod.Kilkist  FROM Nadhod " +
                "INNER JOIN Tovaru ON Nadhod.Artukyl = Tovaru.Artukyl " +
                "WHERE(Nadhod.IdKonteragent = @konterParameter)";

            using (connection = new SqlConnection(connectionString))
            {
                using(SqlCommand command = new SqlCommand(query, connection)) { 
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                     
                   command.Parameters.AddWithValue("konterParameter", cbProduct.SelectedValue);
                 
                   DataTable table = new DataTable();
                   adapter.Fill(table);
                   mgData.DataSource = table;
                    
                }

                }
            }
            ReplaceHeaders();
        }

        private void cbStorage_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

        private void cbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateQuery();
        }
    }
}

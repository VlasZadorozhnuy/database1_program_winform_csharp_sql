﻿namespace ProbaCursApp
{
    partial class ucNadhod
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mgData = new MetroFramework.Controls.MetroGrid();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSkladDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.skladBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.storageDataSet = new ProbaCursApp.StorageDataSet();
            this.idKonteragentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.konteragentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.artukylDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kilkistDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.czinaZaOdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nadhodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.mlDelete = new MetroFramework.Controls.MetroLink();
            this.mlEdit = new MetroFramework.Controls.MetroLink();
            this.mlSave = new MetroFramework.Controls.MetroLink();
            this.mlCancel = new MetroFramework.Controls.MetroLink();
            this.mlNew = new MetroFramework.Controls.MetroLink();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.linkNewProduct = new System.Windows.Forms.LinkLabel();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.cbName = new MetroFramework.Controls.MetroComboBox();
            this.cbStorage = new MetroFramework.Controls.MetroComboBox();
            this.cbKonteragent = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txPrice = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txCount = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.l1 = new MetroFramework.Controls.MetroLabel();
            this.l0 = new MetroFramework.Controls.MetroLabel();
            this.tovaruBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nadhodTableAdapter = new ProbaCursApp.StorageDataSetTableAdapters.NadhodTableAdapter();
            this.konteragentTableAdapter = new ProbaCursApp.StorageDataSetTableAdapters.KonteragentTableAdapter();
            this.skladTableAdapter = new ProbaCursApp.StorageDataSetTableAdapters.SkladTableAdapter();
            this.tovaruTableAdapter = new ProbaCursApp.StorageDataSetTableAdapters.TovaruTableAdapter();
            this.tableAdapterManager = new ProbaCursApp.StorageDataSetTableAdapters.TableAdapterManager();
            this.mpNewProduct = new MetroFramework.Controls.MetroPanel();
            this.bProductCancel = new System.Windows.Forms.Button();
            this.bSaveProduct = new System.Windows.Forms.Button();
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.txVumiry = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txArtukyl = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skladBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storageDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.konteragentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadhodBindingSource)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tovaruBindingSource)).BeginInit();
            this.mpNewProduct.SuspendLayout();
            this.metroPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // mgData
            // 
            this.mgData.AllowUserToResizeRows = false;
            this.mgData.AutoGenerateColumns = false;
            this.mgData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mgData.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mgData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.mgData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mgData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateDataGridViewTextBoxColumn,
            this.idSkladDataGridViewTextBoxColumn,
            this.idKonteragentDataGridViewTextBoxColumn,
            this.artukylDataGridViewTextBoxColumn,
            this.kilkistDataGridViewTextBoxColumn,
            this.czinaZaOdDataGridViewTextBoxColumn});
            this.mgData.DataSource = this.nadhodBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mgData.DefaultCellStyle = dataGridViewCellStyle2;
            this.mgData.EnableHeadersVisualStyles = false;
            this.mgData.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.mgData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.ImeMode = System.Windows.Forms.ImeMode.On;
            this.mgData.Location = new System.Drawing.Point(3, 264);
            this.mgData.MultiSelect = false;
            this.mgData.Name = "mgData";
            this.mgData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.mgData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.mgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mgData.Size = new System.Drawing.Size(754, 254);
            this.mgData.Style = MetroFramework.MetroColorStyle.Silver;
            this.mgData.TabIndex = 2;
            this.mgData.UseCustomBackColor = true;
            this.mgData.UseCustomForeColor = true;
            this.mgData.UseStyleColors = true;
            this.mgData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mgData_KeyDown);
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // idSkladDataGridViewTextBoxColumn
            // 
            this.idSkladDataGridViewTextBoxColumn.DataPropertyName = "IdSklad";
            this.idSkladDataGridViewTextBoxColumn.DataSource = this.skladBindingSource;
            this.idSkladDataGridViewTextBoxColumn.DisplayMember = "NazvaSkladu";
            this.idSkladDataGridViewTextBoxColumn.HeaderText = "IdSklad";
            this.idSkladDataGridViewTextBoxColumn.Name = "idSkladDataGridViewTextBoxColumn";
            this.idSkladDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idSkladDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idSkladDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // skladBindingSource
            // 
            this.skladBindingSource.DataMember = "Sklad";
            this.skladBindingSource.DataSource = this.storageDataSet;
            // 
            // storageDataSet
            // 
            this.storageDataSet.DataSetName = "StorageDataSet";
            this.storageDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // idKonteragentDataGridViewTextBoxColumn
            // 
            this.idKonteragentDataGridViewTextBoxColumn.DataPropertyName = "IdKonteragent";
            this.idKonteragentDataGridViewTextBoxColumn.DataSource = this.konteragentBindingSource;
            this.idKonteragentDataGridViewTextBoxColumn.DisplayMember = "Nazva";
            this.idKonteragentDataGridViewTextBoxColumn.HeaderText = "IdKonteragent";
            this.idKonteragentDataGridViewTextBoxColumn.Name = "idKonteragentDataGridViewTextBoxColumn";
            this.idKonteragentDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idKonteragentDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idKonteragentDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // konteragentBindingSource
            // 
            this.konteragentBindingSource.DataMember = "Konteragent";
            this.konteragentBindingSource.DataSource = this.storageDataSet;
            // 
            // artukylDataGridViewTextBoxColumn
            // 
            this.artukylDataGridViewTextBoxColumn.DataPropertyName = "Artukyl";
            this.artukylDataGridViewTextBoxColumn.HeaderText = "Artukyl";
            this.artukylDataGridViewTextBoxColumn.Name = "artukylDataGridViewTextBoxColumn";
            // 
            // kilkistDataGridViewTextBoxColumn
            // 
            this.kilkistDataGridViewTextBoxColumn.DataPropertyName = "Kilkist";
            this.kilkistDataGridViewTextBoxColumn.HeaderText = "Kilkist";
            this.kilkistDataGridViewTextBoxColumn.Name = "kilkistDataGridViewTextBoxColumn";
            // 
            // czinaZaOdDataGridViewTextBoxColumn
            // 
            this.czinaZaOdDataGridViewTextBoxColumn.DataPropertyName = "CzinaZaOd";
            this.czinaZaOdDataGridViewTextBoxColumn.HeaderText = "CzinaZaOd";
            this.czinaZaOdDataGridViewTextBoxColumn.Name = "czinaZaOdDataGridViewTextBoxColumn";
            // 
            // nadhodBindingSource
            // 
            this.nadhodBindingSource.DataMember = "Nadhod";
            this.nadhodBindingSource.DataSource = this.storageDataSet;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel1.Controls.Add(this.mlDelete);
            this.metroPanel1.Controls.Add(this.mlEdit);
            this.metroPanel1.Controls.Add(this.mlSave);
            this.metroPanel1.Controls.Add(this.mlCancel);
            this.metroPanel1.Controls.Add(this.mlNew);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 220);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(367, 40);
            this.metroPanel1.TabIndex = 5;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // mlDelete
            // 
            this.mlDelete.BackColor = System.Drawing.Color.Transparent;
            this.mlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlDelete.Image = global::ProbaCursApp.Properties.Resources.delete;
            this.mlDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlDelete.ImageSize = 25;
            this.mlDelete.Location = new System.Drawing.Point(294, 0);
            this.mlDelete.Name = "mlDelete";
            this.mlDelete.Size = new System.Drawing.Size(70, 40);
            this.mlDelete.TabIndex = 5;
            this.mlDelete.Text = "Видалити";
            this.mlDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlDelete.UseCustomBackColor = true;
            this.mlDelete.UseSelectable = true;
            this.mlDelete.UseVisualStyleBackColor = false;
            this.mlDelete.Click += new System.EventHandler(this.mlDelete_Click);
            this.mlDelete.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlDelete.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlEdit
            // 
            this.mlEdit.BackColor = System.Drawing.Color.Transparent;
            this.mlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlEdit.Image = global::ProbaCursApp.Properties.Resources.edit;
            this.mlEdit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlEdit.ImageSize = 25;
            this.mlEdit.Location = new System.Drawing.Point(76, 0);
            this.mlEdit.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.mlEdit.Name = "mlEdit";
            this.mlEdit.Size = new System.Drawing.Size(71, 40);
            this.mlEdit.TabIndex = 4;
            this.mlEdit.Text = "Редагувати";
            this.mlEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlEdit.UseCustomBackColor = true;
            this.mlEdit.UseSelectable = true;
            this.mlEdit.UseVisualStyleBackColor = false;
            this.mlEdit.Click += new System.EventHandler(this.mlEdit_Click);
            this.mlEdit.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlEdit.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlSave
            // 
            this.mlSave.BackColor = System.Drawing.Color.Transparent;
            this.mlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlSave.Image = global::ProbaCursApp.Properties.Resources.save;
            this.mlSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlSave.ImageSize = 25;
            this.mlSave.Location = new System.Drawing.Point(225, 0);
            this.mlSave.Name = "mlSave";
            this.mlSave.Size = new System.Drawing.Size(70, 40);
            this.mlSave.TabIndex = 3;
            this.mlSave.Text = "Зберегти";
            this.mlSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlSave.UseCustomBackColor = true;
            this.mlSave.UseSelectable = true;
            this.mlSave.UseVisualStyleBackColor = false;
            this.mlSave.Click += new System.EventHandler(this.mlSave_Click);
            this.mlSave.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlSave.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlCancel
            // 
            this.mlCancel.BackColor = System.Drawing.Color.Transparent;
            this.mlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlCancel.Enabled = false;
            this.mlCancel.Image = global::ProbaCursApp.Properties.Resources.cancel;
            this.mlCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlCancel.ImageSize = 25;
            this.mlCancel.Location = new System.Drawing.Point(150, 0);
            this.mlCancel.Name = "mlCancel";
            this.mlCancel.Size = new System.Drawing.Size(70, 40);
            this.mlCancel.TabIndex = 2;
            this.mlCancel.Text = "Відміна";
            this.mlCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlCancel.UseCustomBackColor = true;
            this.mlCancel.UseSelectable = true;
            this.mlCancel.UseVisualStyleBackColor = false;
            this.mlCancel.Click += new System.EventHandler(this.mlCancel_Click);
            this.mlCancel.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlCancel.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // mlNew
            // 
            this.mlNew.BackColor = System.Drawing.Color.Transparent;
            this.mlNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlNew.Image = global::ProbaCursApp.Properties.Resources.ico_new;
            this.mlNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlNew.ImageSize = 25;
            this.mlNew.Location = new System.Drawing.Point(0, 0);
            this.mlNew.Margin = new System.Windows.Forms.Padding(0);
            this.mlNew.Name = "mlNew";
            this.mlNew.Size = new System.Drawing.Size(70, 40);
            this.mlNew.TabIndex = 0;
            this.mlNew.Text = "Новий";
            this.mlNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlNew.UseCustomBackColor = true;
            this.mlNew.UseSelectable = true;
            this.mlNew.UseVisualStyleBackColor = false;
            this.mlNew.Click += new System.EventHandler(this.mlNew_Click);
            this.mlNew.MouseEnter += new System.EventHandler(this.ButtonMouseEnter);
            this.mlNew.MouseLeave += new System.EventHandler(this.ButtonMouseLeave);
            // 
            // metroPanel3
            // 
            this.metroPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel3.Controls.Add(this.linkNewProduct);
            this.metroPanel3.Controls.Add(this.dateDateTimePicker);
            this.metroPanel3.Controls.Add(this.cbName);
            this.metroPanel3.Controls.Add(this.cbStorage);
            this.metroPanel3.Controls.Add(this.cbKonteragent);
            this.metroPanel3.Controls.Add(this.metroLabel4);
            this.metroPanel3.Controls.Add(this.txPrice);
            this.metroPanel3.Controls.Add(this.metroLabel3);
            this.metroPanel3.Controls.Add(this.txCount);
            this.metroPanel3.Controls.Add(this.metroLabel2);
            this.metroPanel3.Controls.Add(this.metroLabel1);
            this.metroPanel3.Controls.Add(this.l1);
            this.metroPanel3.Controls.Add(this.l0);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(3, 3);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(404, 211);
            this.metroPanel3.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroPanel3.TabIndex = 10;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // linkNewProduct
            // 
            this.linkNewProduct.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkNewProduct.AutoSize = true;
            this.linkNewProduct.BackColor = System.Drawing.Color.White;
            this.linkNewProduct.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkNewProduct.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkNewProduct.Location = new System.Drawing.Point(307, 105);
            this.linkNewProduct.Name = "linkNewProduct";
            this.linkNewProduct.Size = new System.Drawing.Size(86, 15);
            this.linkNewProduct.TabIndex = 19;
            this.linkNewProduct.TabStop = true;
            this.linkNewProduct.Text = "Новий товар?";
            this.linkNewProduct.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkNewProduct_Click);
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.nadhodBindingSource, "Date", true));
            this.dateDateTimePicker.Location = new System.Drawing.Point(101, 6);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateDateTimePicker.TabIndex = 12;
            // 
            // cbName
            // 
            this.cbName.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.nadhodBindingSource, "Artukyl", true));
            this.cbName.FormattingEnabled = true;
            this.cbName.ItemHeight = 23;
            this.cbName.Location = new System.Drawing.Point(101, 94);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(200, 29);
            this.cbName.Style = MetroFramework.MetroColorStyle.Silver;
            this.cbName.TabIndex = 18;
            this.cbName.UseSelectable = true;
            this.cbName.TextChanged += new System.EventHandler(this.Changes);
            // 
            // cbStorage
            // 
            this.cbStorage.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.nadhodBindingSource, "IdSklad", true));
            this.cbStorage.DataSource = this.skladBindingSource;
            this.cbStorage.DisplayMember = "NazvaSkladu";
            this.cbStorage.FormattingEnabled = true;
            this.cbStorage.ItemHeight = 23;
            this.cbStorage.Location = new System.Drawing.Point(101, 63);
            this.cbStorage.Name = "cbStorage";
            this.cbStorage.Size = new System.Drawing.Size(200, 29);
            this.cbStorage.Style = MetroFramework.MetroColorStyle.Silver;
            this.cbStorage.TabIndex = 17;
            this.cbStorage.UseSelectable = true;
            this.cbStorage.ValueMember = "Id";
            this.cbStorage.TextChanged += new System.EventHandler(this.Changes);
            // 
            // cbKonteragent
            // 
            this.cbKonteragent.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.nadhodBindingSource, "IdKonteragent", true));
            this.cbKonteragent.DataSource = this.konteragentBindingSource;
            this.cbKonteragent.DisplayMember = "Nazva";
            this.cbKonteragent.FormattingEnabled = true;
            this.cbKonteragent.ItemHeight = 23;
            this.cbKonteragent.Location = new System.Drawing.Point(101, 32);
            this.cbKonteragent.Name = "cbKonteragent";
            this.cbKonteragent.Size = new System.Drawing.Size(200, 29);
            this.cbKonteragent.Style = MetroFramework.MetroColorStyle.Silver;
            this.cbKonteragent.TabIndex = 16;
            this.cbKonteragent.UseSelectable = true;
            this.cbKonteragent.ValueMember = "Id";
            this.cbKonteragent.TextChanged += new System.EventHandler(this.Changes);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(3, 73);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(45, 19);
            this.metroLabel4.TabIndex = 15;
            this.metroLabel4.Text = "Склад";
            // 
            // txPrice
            // 
            // 
            // 
            // 
            this.txPrice.CustomButton.Image = null;
            this.txPrice.CustomButton.Location = new System.Drawing.Point(176, 1);
            this.txPrice.CustomButton.Name = "";
            this.txPrice.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txPrice.CustomButton.TabIndex = 1;
            this.txPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txPrice.CustomButton.UseSelectable = true;
            this.txPrice.CustomButton.Visible = false;
            this.txPrice.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nadhodBindingSource, "CzinaZaOd", true));
            this.txPrice.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txPrice.Lines = new string[] {
        "txPrice"};
            this.txPrice.Location = new System.Drawing.Point(101, 156);
            this.txPrice.MaxLength = 32767;
            this.txPrice.Name = "txPrice";
            this.txPrice.PasswordChar = '\0';
            this.txPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPrice.SelectedText = "";
            this.txPrice.SelectionLength = 0;
            this.txPrice.SelectionStart = 0;
            this.txPrice.ShortcutsEnabled = true;
            this.txPrice.Size = new System.Drawing.Size(200, 25);
            this.txPrice.Style = MetroFramework.MetroColorStyle.Silver;
            this.txPrice.TabIndex = 12;
            this.txPrice.Text = "txPrice";
            this.txPrice.UseSelectable = true;
            this.txPrice.UseStyleColors = true;
            this.txPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 162);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(95, 19);
            this.metroLabel3.TabIndex = 13;
            this.metroLabel3.Text = "Ціна за од. ($)";
            // 
            // txCount
            // 
            // 
            // 
            // 
            this.txCount.CustomButton.Image = null;
            this.txCount.CustomButton.Location = new System.Drawing.Point(172, 1);
            this.txCount.CustomButton.Name = "";
            this.txCount.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txCount.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txCount.CustomButton.TabIndex = 1;
            this.txCount.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txCount.CustomButton.UseSelectable = true;
            this.txCount.CustomButton.Visible = false;
            this.txCount.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nadhodBindingSource, "Kilkist", true));
            this.txCount.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txCount.Lines = new string[] {
        "txCount"};
            this.txCount.Location = new System.Drawing.Point(101, 125);
            this.txCount.MaxLength = 32767;
            this.txCount.Name = "txCount";
            this.txCount.PasswordChar = '\0';
            this.txCount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txCount.SelectedText = "";
            this.txCount.SelectionLength = 0;
            this.txCount.SelectionStart = 0;
            this.txCount.ShortcutsEnabled = true;
            this.txCount.Size = new System.Drawing.Size(200, 29);
            this.txCount.Style = MetroFramework.MetroColorStyle.Silver;
            this.txCount.TabIndex = 10;
            this.txCount.Text = "txCount";
            this.txCount.UseSelectable = true;
            this.txCount.UseStyleColors = true;
            this.txCount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txCount.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 131);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(58, 19);
            this.metroLabel2.TabIndex = 11;
            this.metroLabel2.Text = "Кількість";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 40);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(84, 19);
            this.metroLabel1.TabIndex = 9;
            this.metroLabel1.Text = "Контерагент";
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Location = new System.Drawing.Point(3, 100);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(90, 19);
            this.l1.TabIndex = 7;
            this.l1.Text = "Назва товару";
            // 
            // l0
            // 
            this.l0.AutoSize = true;
            this.l0.Location = new System.Drawing.Point(3, 9);
            this.l0.Name = "l0";
            this.l0.Size = new System.Drawing.Size(37, 19);
            this.l0.TabIndex = 6;
            this.l0.Text = "Дата";
            // 
            // tovaruBindingSource
            // 
            this.tovaruBindingSource.DataMember = "Tovaru";
            this.tovaruBindingSource.DataSource = this.storageDataSet;
            // 
            // nadhodTableAdapter
            // 
            this.nadhodTableAdapter.ClearBeforeFill = true;
            // 
            // konteragentTableAdapter
            // 
            this.konteragentTableAdapter.ClearBeforeFill = true;
            // 
            // skladTableAdapter
            // 
            this.skladTableAdapter.ClearBeforeFill = true;
            // 
            // tovaruTableAdapter
            // 
            this.tovaruTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.KonteragentTableAdapter = this.konteragentTableAdapter;
            this.tableAdapterManager.NadhodTableAdapter = this.nadhodTableAdapter;
            this.tableAdapterManager.NaSkladiTableAdapter = null;
            this.tableAdapterManager.SkladTableAdapter = this.skladTableAdapter;
            this.tableAdapterManager.TovaruTableAdapter = this.tovaruTableAdapter;
            this.tableAdapterManager.UpdateOrder = ProbaCursApp.StorageDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // mpNewProduct
            // 
            this.mpNewProduct.BackColor = System.Drawing.Color.Silver;
            this.mpNewProduct.Controls.Add(this.bProductCancel);
            this.mpNewProduct.Controls.Add(this.bSaveProduct);
            this.mpNewProduct.Controls.Add(this.metroPanel5);
            this.mpNewProduct.HorizontalScrollbarBarColor = true;
            this.mpNewProduct.HorizontalScrollbarHighlightOnWheel = false;
            this.mpNewProduct.HorizontalScrollbarSize = 10;
            this.mpNewProduct.Location = new System.Drawing.Point(413, 3);
            this.mpNewProduct.Name = "mpNewProduct";
            this.mpNewProduct.Size = new System.Drawing.Size(347, 211);
            this.mpNewProduct.TabIndex = 11;
            this.mpNewProduct.UseCustomBackColor = true;
            this.mpNewProduct.UseStyleColors = true;
            this.mpNewProduct.VerticalScrollbarBarColor = true;
            this.mpNewProduct.VerticalScrollbarHighlightOnWheel = false;
            this.mpNewProduct.VerticalScrollbarSize = 10;
            this.mpNewProduct.Visible = false;
            // 
            // bProductCancel
            // 
            this.bProductCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bProductCancel.FlatAppearance.BorderSize = 0;
            this.bProductCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bProductCancel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bProductCancel.Image = global::ProbaCursApp.Properties.Resources.cancel25;
            this.bProductCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bProductCancel.Location = new System.Drawing.Point(80, 105);
            this.bProductCancel.Name = "bProductCancel";
            this.bProductCancel.Size = new System.Drawing.Size(71, 45);
            this.bProductCancel.TabIndex = 17;
            this.bProductCancel.Text = "Відміна";
            this.bProductCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bProductCancel.UseVisualStyleBackColor = true;
            this.bProductCancel.Click += new System.EventHandler(this.bProductCancel_Click);
            // 
            // bSaveProduct
            // 
            this.bSaveProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bSaveProduct.FlatAppearance.BorderSize = 0;
            this.bSaveProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSaveProduct.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSaveProduct.Image = global::ProbaCursApp.Properties.Resources.save25;
            this.bSaveProduct.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bSaveProduct.Location = new System.Drawing.Point(3, 105);
            this.bSaveProduct.Name = "bSaveProduct";
            this.bSaveProduct.Size = new System.Drawing.Size(71, 45);
            this.bSaveProduct.TabIndex = 16;
            this.bSaveProduct.Text = "Зберегти";
            this.bSaveProduct.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bSaveProduct.UseVisualStyleBackColor = true;
            this.bSaveProduct.Click += new System.EventHandler(this.bSaveProduct_Click);
            // 
            // metroPanel5
            // 
            this.metroPanel5.BackColor = System.Drawing.Color.Silver;
            this.metroPanel5.Controls.Add(this.txVumiry);
            this.metroPanel5.Controls.Add(this.metroLabel5);
            this.metroPanel5.Controls.Add(this.txName);
            this.metroPanel5.Controls.Add(this.metroLabel6);
            this.metroPanel5.Controls.Add(this.txArtukyl);
            this.metroPanel5.Controls.Add(this.metroLabel7);
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(3, 3);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(344, 98);
            this.metroPanel5.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroPanel5.TabIndex = 15;
            this.metroPanel5.UseCustomBackColor = true;
            this.metroPanel5.UseStyleColors = true;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // txVumiry
            // 
            // 
            // 
            // 
            this.txVumiry.CustomButton.Image = null;
            this.txVumiry.CustomButton.Location = new System.Drawing.Point(226, 1);
            this.txVumiry.CustomButton.Name = "";
            this.txVumiry.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txVumiry.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txVumiry.CustomButton.TabIndex = 1;
            this.txVumiry.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txVumiry.CustomButton.UseSelectable = true;
            this.txVumiry.CustomButton.Visible = false;
            this.txVumiry.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txVumiry.Lines = new string[0];
            this.txVumiry.Location = new System.Drawing.Point(90, 65);
            this.txVumiry.MaxLength = 32767;
            this.txVumiry.Name = "txVumiry";
            this.txVumiry.PasswordChar = '\0';
            this.txVumiry.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txVumiry.SelectedText = "";
            this.txVumiry.SelectionLength = 0;
            this.txVumiry.SelectionStart = 0;
            this.txVumiry.ShortcutsEnabled = true;
            this.txVumiry.Size = new System.Drawing.Size(250, 25);
            this.txVumiry.Style = MetroFramework.MetroColorStyle.Silver;
            this.txVumiry.TabIndex = 12;
            this.txVumiry.UseSelectable = true;
            this.txVumiry.UseStyleColors = true;
            this.txVumiry.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txVumiry.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(3, 69);
            this.metroLabel5.Name = "metroLabel1";
            this.metroLabel5.Size = new System.Drawing.Size(75, 19);
            this.metroLabel5.TabIndex = 13;
            this.metroLabel5.Text = "Од. виміру";
            this.metroLabel5.UseCustomBackColor = true;
            // 
            // txName
            // 
            // 
            // 
            // 
            this.txName.CustomButton.Image = null;
            this.txName.CustomButton.Location = new System.Drawing.Point(226, 1);
            this.txName.CustomButton.Name = "";
            this.txName.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txName.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txName.CustomButton.TabIndex = 1;
            this.txName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txName.CustomButton.UseSelectable = true;
            this.txName.CustomButton.Visible = false;
            this.txName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txName.Lines = new string[0];
            this.txName.Location = new System.Drawing.Point(90, 34);
            this.txName.MaxLength = 32767;
            this.txName.Name = "txName";
            this.txName.PasswordChar = '\0';
            this.txName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txName.SelectedText = "";
            this.txName.SelectionLength = 0;
            this.txName.SelectionStart = 0;
            this.txName.ShortcutsEnabled = true;
            this.txName.Size = new System.Drawing.Size(250, 25);
            this.txName.Style = MetroFramework.MetroColorStyle.Silver;
            this.txName.TabIndex = 9;
            this.txName.UseSelectable = true;
            this.txName.UseStyleColors = true;
            this.txName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(3, 38);
            this.metroLabel6.Name = "l1";
            this.metroLabel6.Size = new System.Drawing.Size(45, 19);
            this.metroLabel6.TabIndex = 11;
            this.metroLabel6.Text = "Назва";
            this.metroLabel6.UseCustomBackColor = true;
            // 
            // txArtukyl
            // 
            // 
            // 
            // 
            this.txArtukyl.CustomButton.Image = null;
            this.txArtukyl.CustomButton.Location = new System.Drawing.Point(226, 1);
            this.txArtukyl.CustomButton.Name = "";
            this.txArtukyl.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txArtukyl.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txArtukyl.CustomButton.TabIndex = 1;
            this.txArtukyl.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txArtukyl.CustomButton.UseSelectable = true;
            this.txArtukyl.CustomButton.Visible = false;
            this.txArtukyl.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txArtukyl.Lines = new string[0];
            this.txArtukyl.Location = new System.Drawing.Point(90, 5);
            this.txArtukyl.MaxLength = 32767;
            this.txArtukyl.Name = "txArtukyl";
            this.txArtukyl.PasswordChar = '\0';
            this.txArtukyl.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txArtukyl.SelectedText = "";
            this.txArtukyl.SelectionLength = 0;
            this.txArtukyl.SelectionStart = 0;
            this.txArtukyl.ShortcutsEnabled = true;
            this.txArtukyl.Size = new System.Drawing.Size(250, 25);
            this.txArtukyl.Style = MetroFramework.MetroColorStyle.Silver;
            this.txArtukyl.TabIndex = 8;
            this.txArtukyl.UseSelectable = true;
            this.txArtukyl.UseStyleColors = true;
            this.txArtukyl.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txArtukyl.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(3, 9);
            this.metroLabel7.Name = "l0";
            this.metroLabel7.Size = new System.Drawing.Size(57, 19);
            this.metroLabel7.TabIndex = 10;
            this.metroLabel7.Text = "Артикул";
            this.metroLabel7.UseCustomBackColor = true;
            // 
            // ucNadhod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.mpNewProduct);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.mgData);
            this.Name = "ucNadhod";
            this.Size = new System.Drawing.Size(780, 538);
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.UseStyleColors = true;
            this.Load += new System.EventHandler(this.ucNadhod_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skladBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storageDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.konteragentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nadhodBindingSource)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tovaruBindingSource)).EndInit();
            this.mpNewProduct.ResumeLayout(false);
            this.metroPanel5.ResumeLayout(false);
            this.metroPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroGrid mgData;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLink mlSave;
        private MetroFramework.Controls.MetroLink mlCancel;
        private MetroFramework.Controls.MetroLink mlNew;
        private MetroFramework.Controls.MetroLink mlEdit;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroLabel l1;
        private MetroFramework.Controls.MetroLabel l0;
        private MetroFramework.Controls.MetroLink mlDelete;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txPrice;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txCount;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cbKonteragent;
        private MetroFramework.Controls.MetroComboBox cbStorage;
        private MetroFramework.Controls.MetroComboBox cbName;
        private System.Windows.Forms.BindingSource nadhodBindingSource;
        private StorageDataSet storageDataSet;
        private System.Windows.Forms.BindingSource tovaruBindingSource;
        private System.Windows.Forms.BindingSource skladBindingSource;
        private System.Windows.Forms.BindingSource konteragentBindingSource;
        private StorageDataSetTableAdapters.NadhodTableAdapter nadhodTableAdapter;
        private StorageDataSetTableAdapters.KonteragentTableAdapter konteragentTableAdapter;
        private StorageDataSetTableAdapters.SkladTableAdapter skladTableAdapter;
        private StorageDataSetTableAdapters.TovaruTableAdapter tovaruTableAdapter;
        private StorageDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idSkladDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idKonteragentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn artukylDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kilkistDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn czinaZaOdDataGridViewTextBoxColumn;
        private System.Windows.Forms.LinkLabel linkNewProduct;
        private MetroFramework.Controls.MetroPanel mpNewProduct;
        private MetroFramework.Controls.MetroPanel metroPanel5;
        private MetroFramework.Controls.MetroTextBox txVumiry;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txName;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txArtukyl;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.Button bSaveProduct;
        private System.Windows.Forms.Button bProductCancel;
    }
}

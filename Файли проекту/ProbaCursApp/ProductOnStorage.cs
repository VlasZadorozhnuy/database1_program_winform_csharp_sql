﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace ProbaCursApp
{
    public partial class ProductOnStorage : UserControl
    {
        SqlConnection connection;
        string connectionString;
         DataTable table;

        public ProductOnStorage()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["ProbaCursApp.Properties.Settings.StorageConnectionString"].ConnectionString;
        }

        private void ProductOnStorage_Load(object sender, EventArgs e)
        {
            
            fmMain.Instance.Text = "        Розміщення товару";
            fmMain.Instance.Refresh();
            PopulateProducts();
            mgData.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
        }

        private void PopulateProducts() {
            using (connection = new SqlConnection(connectionString) ) {
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Tovaru", connection)) {
                    DataTable productTable = new DataTable();
                    adapter.Fill(productTable);
                    cbProduct.DisplayMember = "Nazva";
                    cbProduct.ValueMember = "Artukyl";
                    cbProduct.DataSource = productTable;
                    cbProduct.SelectedIndex = 0;
                }

            }
        }

        private void ReplaceHeaders()
        {
            mgData.Columns[0].HeaderText = "Склад";
            mgData.Columns[1].HeaderText = "Кількість";
        }

        private void PopulateStorages()
        {
            //feat Vova DRonenko
            string query = "SELECT Sklad.NazvaSkladu, SUM(Nadhod.Kilkist)  FROM Nadhod " +
                "INNER JOIN Sklad ON Nadhod.IdSklad = Sklad.Id " +
                "WHERE(Nadhod.Artukyl = @artukylparameter) GROUP BY Sklad.NazvaSkladu";

            using (connection = new SqlConnection(connectionString))
            {
                using(SqlCommand command = new SqlCommand(query, connection)) { 
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                     
                   command.Parameters.AddWithValue("artukylparameter", cbProduct.SelectedValue);
                   
                   table = new DataTable();
                   adapter.Fill(table);
                   mgData.DataSource = table;
                    
                }

                }
            }
            ReplaceHeaders();
        }

        private void cbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateStorages();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace ProbaCursApp
{
   // public partial class ucOnStorage : MetroFramework.Controls.MetroUserControl;
    public partial class ucOnStorage : MetroFramework.Controls.MetroUserControl
    {
        bool tableEdited=false;

        #region Init

        private void ReplaceHeaders() {
            mgData.Columns[0].HeaderText = "Назва складу";
            mgData.Columns[1].HeaderText = "МВО";
        }

        private void AddComboboxItems()
        {
            for (int i=0; i < mgData.Columns.Count; i++) {
                cbSearch.Items.Add(mgData.Columns[i].HeaderText);
           }
            cbSearch.SelectedIndex = 0;
        }

        public ucOnStorage()
        {
            InitializeComponent();
        }

        private void ucOnStorage_Load(object sender, EventArgs e)
        {
            fmMain.Instance.Text = "        На складі";
            fmMain.Instance.Refresh();

            this.skladTableAdapter.Fill(this.storageDataSet.Sklad);
            Edit(false);
            //new headers text in grid
            ReplaceHeaders();
            //add headers values in combobox Search
            AddComboboxItems();
        }
        #endregion
        #region navigation logic

        private void Edit(bool value) {
           
            txArtykul.Enabled =  value;
            mlCancel.Enabled = true;
        }

        private void TableEdited(bool value)
        {
            mlCancel.Enabled = value;
            tableEdited = value;
            fmMain.Instance.LAsterix.Visible = value;
        }

        private void mlNew_Click(object sender, EventArgs e)
        {
            try
            {
                
                Edit(true);
                storageDataSet.Sklad.AddSkladRow(storageDataSet.Sklad.NewSkladRow());
                skladBindingSource.MoveLast();
                //txDate.Focus();
                TableEdited(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Sklad.RejectChanges();
                throw;
            }
        }

        private void mlEdit_Click(object sender, EventArgs e)
        {
            Edit(true);
            //txDate.Focus();
            TableEdited(true);
        }

        private void mlCancel_Click(object sender, EventArgs e)
        {
            Edit(false);
            skladBindingSource.CancelEdit();
            skladBindingSource.ResetBindings(false);
            skladTableAdapter.Update(storageDataSet.Sklad);
            mgData.Refresh();
            TableEdited(false);
        }

        private void mlSave_Click(object sender, EventArgs e)
        {
            try
            {
                Edit(false);
                skladBindingSource.EndEdit();
                skladTableAdapter.Update(storageDataSet.Sklad);
                mgData.Refresh();
                TableEdited(false);
                MessageBox.Show("Збережено", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Error);
                storageDataSet.Sklad.RejectChanges();
                throw;
            }
        }

        private void Search() {
            if (!string.IsNullOrEmpty(txSearch.Text))
            {
                //get table name where we gonna search
                string dbTableName = mgData.Columns[cbSearch.SelectedIndex].DataPropertyName;
                //set filter to display in datagrid
                skladBindingSource.Filter = string.Format("{0} LIKE '*{1}*'", dbTableName, txSearch.Text);
                // skladBindingSource.Filter = string.Format("ImyaSklad LIKE '*{0}*' OR MVO LIKE '*{1}*'", txName.Text,txMvo.Text);
            }
            else
            {
                skladBindingSource.Filter = string.Empty;
            }
        }

        private void txSearch_TextChanged(object sender, EventArgs e)
        {
            Search();
        }
        //delete
        private void mgData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)//delete
            {
                if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    skladBindingSource.RemoveCurrent();

                }
            }
        }

        private void cbSearch_TextChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void mlSearch_Click(object sender, EventArgs e)
        {
            Search();
        }
        
        private void mlDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ви впевнені що хочете видалити запис?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                skladBindingSource.RemoveCurrent();
                TableEdited(true);
            }
        }

        #endregion

        private void ButtonMouseLeave(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.Transparent;
        }

        private void ButtonMouseEnter(object sender, EventArgs e)
        {
            MetroLink mt = sender as MetroLink;
            mt.BackColor = Color.FromArgb(215, 215, 215);
        }

        private void txName_Click(object sender, EventArgs e)
        {

        }
    }
}

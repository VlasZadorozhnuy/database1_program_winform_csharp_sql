﻿namespace ProbaCursApp
{
    partial class ucLogin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lStorage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txLogin = new System.Windows.Forms.TextBox();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.bLogin = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lWarning = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lStorage
            // 
            this.lStorage.AutoSize = true;
            this.lStorage.BackColor = System.Drawing.Color.Transparent;
            this.lStorage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lStorage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lStorage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lStorage.Location = new System.Drawing.Point(221, 212);
            this.lStorage.Name = "lStorage";
            this.lStorage.Size = new System.Drawing.Size(49, 21);
            this.lStorage.TabIndex = 21;
            this.lStorage.Text = "Логін";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(221, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 21);
            this.label2.TabIndex = 23;
            this.label2.Text = "Пароль";
            // 
            // txLogin
            // 
            this.txLogin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txLogin.Location = new System.Drawing.Point(293, 209);
            this.txLogin.Name = "txLogin";
            this.txLogin.Size = new System.Drawing.Size(165, 29);
            this.txLogin.TabIndex = 25;
            // 
            // txPassword
            // 
            this.txPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txPassword.Location = new System.Drawing.Point(293, 244);
            this.txPassword.Name = "txPassword";
            this.txPassword.PasswordChar = '*';
            this.txPassword.Size = new System.Drawing.Size(165, 29);
            this.txPassword.TabIndex = 26;
            // 
            // bLogin
            // 
            this.bLogin.BackColor = System.Drawing.Color.Silver;
            this.bLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bLogin.FlatAppearance.BorderSize = 0;
            this.bLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLogin.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLogin.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bLogin.Location = new System.Drawing.Point(293, 279);
            this.bLogin.Name = "bLogin";
            this.bLogin.Size = new System.Drawing.Size(165, 45);
            this.bLogin.TabIndex = 27;
            this.bLogin.Text = "Увійти";
            this.bLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bLogin.UseVisualStyleBackColor = false;
            this.bLogin.Click += new System.EventHandler(this.bLogin_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::ProbaCursApp.Properties.Resources.iconpeoplecircle;
            this.pictureBox1.Location = new System.Drawing.Point(293, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(165, 165);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // lWarning
            // 
            this.lWarning.BackColor = System.Drawing.Color.Transparent;
            this.lWarning.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lWarning.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lWarning.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lWarning.ForeColor = System.Drawing.Color.DarkRed;
            this.lWarning.Location = new System.Drawing.Point(221, 327);
            this.lWarning.Name = "lWarning";
            this.lWarning.Size = new System.Drawing.Size(305, 123);
            this.lWarning.TabIndex = 29;
            // 
            // ucLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::ProbaCursApp.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.lWarning);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bLogin);
            this.Controls.Add(this.txPassword);
            this.Controls.Add(this.txLogin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lStorage);
            this.Name = "ucLogin";
            this.Size = new System.Drawing.Size(760, 520);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lStorage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txLogin;
        private System.Windows.Forms.TextBox txPassword;
        private System.Windows.Forms.Button bLogin;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lWarning;
    }
}

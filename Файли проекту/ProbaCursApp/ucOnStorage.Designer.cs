﻿namespace ProbaCursApp
{
    partial class ucOnStorage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.txName = new MetroFramework.Controls.MetroTextBox();
            this.l0 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.cbSearch = new MetroFramework.Controls.MetroComboBox();
            this.txSearch = new MetroFramework.Controls.MetroTextBox();
            this.mlSearch = new MetroFramework.Controls.MetroLink();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.mlDelete = new MetroFramework.Controls.MetroLink();
            this.mlEdit = new MetroFramework.Controls.MetroLink();
            this.mlSave = new MetroFramework.Controls.MetroLink();
            this.mlCancel = new MetroFramework.Controls.MetroLink();
            this.mlNew = new MetroFramework.Controls.MetroLink();
            this.mgData = new MetroFramework.Controls.MetroGrid();
            this.metroPanel3.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel3
            // 
            this.metroPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel3.Controls.Add(this.txName);
            this.metroPanel3.Controls.Add(this.l0);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(2, 3);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(367, 119);
            this.metroPanel3.TabIndex = 22;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // txName
            // 
            // 
            // 
            // 
            this.txName.CustomButton.Image = null;
            this.txName.CustomButton.Location = new System.Drawing.Point(250, 1);
            this.txName.CustomButton.Name = "";
            this.txName.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.txName.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txName.CustomButton.TabIndex = 1;
            this.txName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txName.CustomButton.UseSelectable = true;
            this.txName.CustomButton.Visible = false;
            this.txName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txName.Lines = new string[] {
        "txKonteragent"};
            this.txName.Location = new System.Drawing.Point(90, 5);
            this.txName.MaxLength = 32767;
            this.txName.Name = "txName";
            this.txName.PasswordChar = '\0';
            this.txName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txName.SelectedText = "";
            this.txName.SelectionLength = 0;
            this.txName.SelectionStart = 0;
            this.txName.ShortcutsEnabled = true;
            this.txName.Size = new System.Drawing.Size(274, 25);
            this.txName.Style = MetroFramework.MetroColorStyle.Silver;
            this.txName.TabIndex = 8;
            this.txName.Text = "txKonteragent";
            this.txName.UseSelectable = true;
            this.txName.UseStyleColors = true;
            this.txName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txName.Click += new System.EventHandler(this.txName_Click);
            // 
            // l0
            // 
            this.l0.AutoSize = true;
            this.l0.Location = new System.Drawing.Point(3, 9);
            this.l0.Name = "l0";
            this.l0.Size = new System.Drawing.Size(45, 19);
            this.l0.TabIndex = 10;
            this.l0.Text = "Назва";
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel2.Controls.Add(this.cbSearch);
            this.metroPanel2.Controls.Add(this.txSearch);
            this.metroPanel2.Controls.Add(this.mlSearch);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(405, 124);
            this.metroPanel2.Margin = new System.Windows.Forms.Padding(1);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(353, 40);
            this.metroPanel2.TabIndex = 21;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // cbSearch
            // 
            this.cbSearch.FormattingEnabled = true;
            this.cbSearch.ItemHeight = 23;
            this.cbSearch.Location = new System.Drawing.Point(223, 8);
            this.cbSearch.Name = "cbSearch";
            this.cbSearch.Size = new System.Drawing.Size(121, 29);
            this.cbSearch.TabIndex = 12;
            this.cbSearch.UseSelectable = true;
            // 
            // txSearch
            // 
            // 
            // 
            // 
            this.txSearch.CustomButton.Image = null;
            this.txSearch.CustomButton.Location = new System.Drawing.Point(116, 1);
            this.txSearch.CustomButton.Name = "";
            this.txSearch.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Silver;
            this.txSearch.CustomButton.TabIndex = 1;
            this.txSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txSearch.CustomButton.UseSelectable = true;
            this.txSearch.CustomButton.Visible = false;
            this.txSearch.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txSearch.Lines = new string[0];
            this.txSearch.Location = new System.Drawing.Point(73, 8);
            this.txSearch.MaxLength = 32767;
            this.txSearch.Name = "txSearch";
            this.txSearch.PasswordChar = '\0';
            this.txSearch.PromptText = "Що шукати?..";
            this.txSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txSearch.SelectedText = "";
            this.txSearch.SelectionLength = 0;
            this.txSearch.SelectionStart = 0;
            this.txSearch.ShortcutsEnabled = true;
            this.txSearch.Size = new System.Drawing.Size(144, 29);
            this.txSearch.TabIndex = 11;
            this.txSearch.UseSelectable = true;
            this.txSearch.WaterMark = "Що шукати?..";
            this.txSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mlSearch
            // 
            this.mlSearch.BackColor = System.Drawing.Color.Transparent;
            this.mlSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlSearch.Image = global::ProbaCursApp.Properties.Resources.search;
            this.mlSearch.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlSearch.ImageSize = 25;
            this.mlSearch.Location = new System.Drawing.Point(0, 0);
            this.mlSearch.Margin = new System.Windows.Forms.Padding(0);
            this.mlSearch.Name = "mlSearch";
            this.mlSearch.Size = new System.Drawing.Size(70, 40);
            this.mlSearch.TabIndex = 7;
            this.mlSearch.Text = "Пошук";
            this.mlSearch.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlSearch.UseCustomBackColor = true;
            this.mlSearch.UseSelectable = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.metroPanel1.Controls.Add(this.mlDelete);
            this.metroPanel1.Controls.Add(this.mlEdit);
            this.metroPanel1.Controls.Add(this.mlSave);
            this.metroPanel1.Controls.Add(this.mlCancel);
            this.metroPanel1.Controls.Add(this.mlNew);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(2, 128);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(367, 40);
            this.metroPanel1.TabIndex = 20;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // mlDelete
            // 
            this.mlDelete.BackColor = System.Drawing.Color.Transparent;
            this.mlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlDelete.Image = global::ProbaCursApp.Properties.Resources.delete;
            this.mlDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlDelete.ImageSize = 25;
            this.mlDelete.Location = new System.Drawing.Point(294, 0);
            this.mlDelete.Name = "mlDelete";
            this.mlDelete.Size = new System.Drawing.Size(70, 40);
            this.mlDelete.TabIndex = 5;
            this.mlDelete.Text = "Видалити";
            this.mlDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlDelete.UseCustomBackColor = true;
            this.mlDelete.UseSelectable = true;
            // 
            // mlEdit
            // 
            this.mlEdit.BackColor = System.Drawing.Color.Transparent;
            this.mlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlEdit.Image = global::ProbaCursApp.Properties.Resources.edit;
            this.mlEdit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlEdit.ImageSize = 25;
            this.mlEdit.Location = new System.Drawing.Point(76, 0);
            this.mlEdit.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.mlEdit.Name = "mlEdit";
            this.mlEdit.Size = new System.Drawing.Size(71, 40);
            this.mlEdit.TabIndex = 4;
            this.mlEdit.Text = "Редагувати";
            this.mlEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlEdit.UseCustomBackColor = true;
            this.mlEdit.UseSelectable = true;
            // 
            // mlSave
            // 
            this.mlSave.BackColor = System.Drawing.Color.Transparent;
            this.mlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlSave.Image = global::ProbaCursApp.Properties.Resources.save;
            this.mlSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlSave.ImageSize = 25;
            this.mlSave.Location = new System.Drawing.Point(225, 0);
            this.mlSave.Name = "mlSave";
            this.mlSave.Size = new System.Drawing.Size(70, 40);
            this.mlSave.TabIndex = 3;
            this.mlSave.Text = "Зберегти";
            this.mlSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlSave.UseCustomBackColor = true;
            this.mlSave.UseSelectable = true;
            // 
            // mlCancel
            // 
            this.mlCancel.BackColor = System.Drawing.Color.Transparent;
            this.mlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlCancel.Enabled = false;
            this.mlCancel.Image = global::ProbaCursApp.Properties.Resources.cancel;
            this.mlCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlCancel.ImageSize = 25;
            this.mlCancel.Location = new System.Drawing.Point(150, 0);
            this.mlCancel.Name = "mlCancel";
            this.mlCancel.Size = new System.Drawing.Size(70, 40);
            this.mlCancel.TabIndex = 2;
            this.mlCancel.Text = "Відміна";
            this.mlCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlCancel.UseCustomBackColor = true;
            this.mlCancel.UseSelectable = true;
            // 
            // mlNew
            // 
            this.mlNew.BackColor = System.Drawing.Color.Transparent;
            this.mlNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mlNew.Image = global::ProbaCursApp.Properties.Resources._new;
            this.mlNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.mlNew.ImageSize = 25;
            this.mlNew.Location = new System.Drawing.Point(0, 0);
            this.mlNew.Margin = new System.Windows.Forms.Padding(0);
            this.mlNew.Name = "mlNew";
            this.mlNew.Size = new System.Drawing.Size(70, 40);
            this.mlNew.TabIndex = 0;
            this.mlNew.Text = "Новий";
            this.mlNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.mlNew.UseCustomBackColor = true;
            this.mlNew.UseSelectable = true;
            // 
            // mgData
            // 
            this.mgData.AllowUserToResizeRows = false;
            this.mgData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mgData.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mgData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.mgData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mgData.DefaultCellStyle = dataGridViewCellStyle2;
            this.mgData.EnableHeadersVisualStyles = false;
            this.mgData.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.mgData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mgData.ImeMode = System.Windows.Forms.ImeMode.On;
            this.mgData.Location = new System.Drawing.Point(2, 168);
            this.mgData.MultiSelect = false;
            this.mgData.Name = "mgData";
            this.mgData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mgData.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.mgData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.mgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mgData.Size = new System.Drawing.Size(754, 349);
            this.mgData.Style = MetroFramework.MetroColorStyle.Silver;
            this.mgData.TabIndex = 19;
            this.mgData.UseCustomBackColor = true;
            this.mgData.UseCustomForeColor = true;
            this.mgData.UseStyleColors = true;
            // 
            // ucOnStorage
            // 
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.mgData);
            this.Name = "ucOnStorage";
            this.Size = new System.Drawing.Size(760, 520);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mgData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroTextBox txName;
        private MetroFramework.Controls.MetroLabel l0;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroComboBox cbSearch;
        private MetroFramework.Controls.MetroTextBox txSearch;
        private MetroFramework.Controls.MetroLink mlSearch;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLink mlDelete;
        private MetroFramework.Controls.MetroLink mlEdit;
        private MetroFramework.Controls.MetroLink mlSave;
        private MetroFramework.Controls.MetroLink mlCancel;
        private MetroFramework.Controls.MetroLink mlNew;
        private MetroFramework.Controls.MetroGrid mgData;
    }
}

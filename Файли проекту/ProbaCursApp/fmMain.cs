﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace ProbaCursApp
{
    public partial class fmMain : MetroFramework.Forms.MetroForm
    {
        static fmMain _instance;

        private ucDashboard uc;

        public static fmMain Instance {
            get {
                if (_instance == null)
                    _instance = new fmMain();
                return _instance;
            }
        }

        public Panel MetroContainer {
            get {return mpPanel;}
            set {mpPanel = value;}
        }

        public MetroLink MetroBack {
            get { return mlBack; }
            set { mlBack = value; }
        }

        public MetroLabel LAsterix
        {
            get { return lAsterix; }
            set { lAsterix = value; }
        }

        public fmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'storageDataSet.Sklad' table. You can move, or remove it, as needed.
            this.skladTableAdapter.Fill(this.storageDataSet.Sklad);


            mlBack.Visible = false;
            lAsterix.Visible = false;
            _instance = this;
            LoadUcLogin();
           }

        public void LoadUcLogin()
        {
            mpPanel.Controls.Clear();
            ucLogin uc = new ucLogin();
            uc.Dock = DockStyle.Fill;
            mpPanel.Controls.Add(uc);
            
        }

        private void mlBack_Click(object sender, EventArgs e)
        {
            if (lAsterix.Visible == true)
            {
                if (MessageBox.Show("Ви впевнені що хочете вийди без збереження?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

            }
            mpPanel.Controls["ucDashboard"].BringToFront();
            this.Text = "        Облік товарів на складах";
            mlBack.Visible = false;
            lAsterix.Visible = false;
            this.Refresh();
        }

        public void LoadDashboard(int permission, string comment) {
            uc = new ucDashboard();
            uc.Dock = DockStyle.Fill;
            mpPanel.Controls.Add(uc);
            //comment
            uc.lComment.Text = comment;
            uc.lComment.Visible = true;

            CustomizeDashboard(permission);

            mpPanel.Controls["ucDashboard"].BringToFront();
            this.Text = "        Облік товарів на складах";
            mlBack.Visible = false;
            lAsterix.Visible = false;
            this.Refresh();
        }

        private void CustomizeDashboard(int permission) {
            switch (permission)
            {
                case (1): 
                    //
                        uc.mlStorage.Visible = true;
                        uc.mlKonteragent.Visible = true;
                        uc.mlNadhod.Visible = true;
                        uc.mlProduct.Visible = true;
                        //query
                        uc.linkProduct.Visible = true;
                        uc.linkOnStorage.Visible = true;
                        uc.linkKonteragent.Visible = true;
                        uc.linkNadhod.Visible = true;                      
                    break;
                case (2):
                    //
                    uc.mlStorage.Visible = true;
                    uc.mlKonteragent.Visible = true;
                    uc.mlNadhod.Visible = true;
                    uc.mlProduct.Visible = true;
                    //query
                    uc.linkProduct.Visible = true;
                    uc.linkOnStorage.Visible = true;
                    uc.linkKonteragent.Visible = true;
                    uc.linkNadhod.Visible = true;
                    break;
                case (3):
                    //
                    uc.mlStorage.Visible = false;
                    uc.mlKonteragent.Visible = false;
                    uc.mlNadhod.Visible = true;
                    uc.mlProduct.Visible = false;
                    //query
                    uc.linkProduct.Visible = true;
                    uc.linkOnStorage.Visible = true;
                    uc.linkKonteragent.Visible = true;
                    uc.linkNadhod.Visible = true;
                    break;


                default: break;
            }
            
        }

    }
}
